if run_name =="debris"
    ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine.csv", ',',skipstart=1)
    ice_surf_spl, ice_base_spl, ice_ub_spl, ice_qw_spl,ice_sdeb_spl = ice_model_load(ice_data,5,1550,4)
    pg = Mountain(pg,
                  domain = (0.0,maximum(ice_data[1:1550,1])),
                  )

elseif run_name=="clean"
    ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_free_example_for_ian.csv", ',',skipstart=1)
    ice_surf_spl, ice_base_spl, ice_ub_spl, ice_qw_spl,ice_sdeb_spl = ice_model_load(ice_data,1,155,2)
    
    pg = Mountain(pg,
                  domain = (0.0,maximum(ice_data[1:155,1])),
                  )
elseif run_name =="debris_cleanmelt"
    ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine.csv", ',',skipstart=1)
    ice_surf_spl, ice_base_spl, ice_ub_spl,  ice_qw_spl,ice_sdeb_spl = ice_model_load(ice_data,5,1550,4)
    pg = Mountain(pg,
                  domain = (0.0,maximum(ice_data[1:1550,1])),
                  )

    ice_data_clean = DelimitedFiles.readdlm("$(ice_data_loc)debris_free_example_for_ian.csv", ',',skipstart=1)
    _, _, _, ice_qw_spl,_ = ice_model_load(ice_data_clean,1,155,2)
end
