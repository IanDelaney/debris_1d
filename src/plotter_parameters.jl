using Parameters
using PyPlot
import Roots: find_zero
using LaTeXStrings
using Dates
using JLD2
using StatsBase
using DelimitedFiles
using HypothesisTests
include("post_proc_funs.jl")
#################"
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
end
%
yr= 365*86400
test= @isdefined Qbs_catch_d
run_name=["debris" "clean"]

# # # load ice data
if test == false     
    include("ensemble_load.jl")
    
    @load "$(fp_load)model_setup.jld2" sgrid zb_clean zb_debris zs_clean zs_debris ṁw_debris ṁw_clean Qw_clean Qw_debris ∂ϕ_∂x_debris ∂ϕ_∂x_clean ub_debris ub_clean surf_debris

    @load "Parameters_clean.jld2" ϵ̇_l Kg_l l_er_l Dm_l
    ė_l_clean = ϵ̇_l; Kg_l_clean= Kg_l; l_er_l_clean=l_er_l; Dm_l_clean=Dm_l

    @load "Parameters_debris.jld2" ϵ̇_l Kg_l l_er_l Dm_l
    ė_l_debris = ϵ̇_l; Kg_l_debris = Kg_l; l_er_l_debris = l_er_l; Dm_l_debris =Dm_l


    ϵ̇= [ė_l_debris[1:500]; ė_l_clean[1:500]]; Dm = [Dm_l_debris[1:500]; Dm_l_clean[1:500]]
    l_er= [l_er_l_debris[1:500]; l_er_l_clean[1:500]]; Kg= [Kg_l_debris[1:500]; Kg_l_clean[1:500]]
end


###############
dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )
        
       
    else
        push!(dep,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )

    end
end
trans= vcat(trans...)
dep =vcat(dep...)


figure(2,figsize=(9.6,6)); clf()
subplot2grid((8,4), (0,0), rowspan=2, colspan=2)
hist(ϵ̇*1e3,bins=25)
text(2,125, "a")
text(7.5,125, "All model runs")
#xlabel("Potential Bedrock \n  Erosion Rate (mm a⁻¹)")
xticks([])
#ylim([1,150])
xlim([0.95, 20])

subplot2grid((8,4), (0,2), rowspan=2, colspan=2)
hist(Dm*10,bins=25)
text(.25,65, "b")
##xlabel("Mean sediment size \n (Dₘ; cm)")
xlim([0.105, 2])
#text(5,65,"Mean sediment size ")
#ylim([1,75])
xticks([])



subplot2grid((8,4), (2,0), rowspan=2, colspan=2)
hist(trans[:,1],bins=25, color="lightpink", label="no bulb, n:$(length(trans[:,1]))",alpha=.75)
hist(  dep[:,1],bins=25, color="mediumseagreen", label="bulb, n:$(length(dep[:,1]))",alpha=.75)
xlabel("Potential Bedrock \n  Erosion Rate (mm a⁻¹)")
text(7.5,163, L"$DEBRIS$ runs")
xlim([0.95, 20])
legend()
text(2,163, "c")


subplot2grid((8,4), (2,2), rowspan=2, colspan=2)
hist(trans[:,end]*10,bins=25, color="lightpink", label="no bulb",alpha=.75)
hist(  dep[:,end]*10,bins=25, color="mediumseagreen", label="bulb",alpha=.75)
xlabel("Mean sediment size \n (Dₘ; cm)")
xlim([0.105, 2])
text(.25,32, "d")


subplot2grid((8,4), (5,1), rowspan=2, colspan=2)
scatter(l_er,Kg, c=ϵ̇*1e3, cmap= "viridis");yscale("log");cp=colorbar()
xlabel("Erosional exponent \n (lₑᵣ)")
ylabel(latexstring("Erosional constant \n (K\$_g\$; m\$^{1-l_{er}}\$ a\$^{l_{er}-1}\$)"))
cp.set_label("Potential Bedrock \n  Erosion Rate \n (mm a⁻¹)",rotation=270,labelpad=35)
text(1.4,4.5e-5, "e")

tight_layout()
subplots_adjust(hspace=0)

savefig("$(fp_save)model_parameters.pdf")


error()


