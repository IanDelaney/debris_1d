


hts_mean=[];Qbs_mean=[];ϵ̇_mean=[];ṁt_mean=[];H20_velocity=[]; 

Qbs_catch_d=[];ϵ̇_catch_d=[];ṁt_catch_d=[];Dm_d=[];Kg_d=[];l_er_d=[]
Qbs_rate_d=[]; ϵ̇_rate_d=[];ṁt_rate_d=[];hts_bulb_d=[]

Qbs_catch_c=[];ϵ̇_catch_c=[];ṁt_catch_c=[];Dm_c=[];Kg_c=[];l_er_c=[]
Qbs_rate_c=[];  ϵ̇_rate_c=[];ṁt_rate_c=[];hts_bulb_c=[]


hts_moy=zeros(349,length(run_name));Qbs_moy=zeros(350,length(run_name));Qbes_moy=zeros(350,length(run_name));ϵ̇_moy=zeros(350,length(run_name));ṁt_moy=zeros(349,length(run_name))
hts_min=zeros(349,length(run_name));Qbs_min=zeros(350,length(run_name));Qbes_min=zeros(350,length(run_name));ϵ̇_min=zeros(350,length(run_name));ṁt_min=zeros(349,length(run_name))
hts_max=zeros(349,length(run_name));Qbs_max=zeros(350,length(run_name));Qbes_max=zeros(350,length(run_name));ϵ̇_max=zeros(350,length(run_name));ṁt_max=zeros(349,length(run_name))




for i = 1:length(run_name)
    global  hts_block, Qbs_block, Qbes_block, ϵ̇_block, ṁt_block, Dm_block, zs_block, zb_block, H20_velocity_block, ∂ϕ_∂x_block, Kg_block, l_er_block =return_run(fp_load,run_name[i])

    
    push!(hts_mean,mean(hts_block,dims=2))
    push!(Qbs_mean,mean(Qbs_block,dims=2))
    push!(ϵ̇_mean,mean(ϵ̇_block,dims=2))
    push!(ṁt_mean,mean(ṁt_block,dims=2))
    push!(H20_velocity,H20_velocity_block)

    for j = 1:size(hts_block,1)
        hts_min[j,i]= percentile(hts_block[j,:],25)
        hts_moy[j,i]= percentile(hts_block[j,:],50)
        hts_max[j,i]= percentile(hts_block[j,:],75)

        ṁt_min[j,i]= percentile(ṁt_block[j,:],25)
        ṁt_moy[j,i]= percentile(ṁt_block[j,:],50)
        ṁt_max[j,i]= percentile(ṁt_block[j,:],75)
    end
    
    for j = 1:size(Qbs_block,1)
        Qbs_min[j,i]= percentile(Qbs_block[j,:],25)
        Qbs_moy[j,i]= percentile(Qbs_block[j,:],50)
        Qbs_max[j,i]= percentile(Qbs_block[j,:],75)

        Qbes_min[j,i]= percentile(Qbes_block[j,:],25)
        Qbes_moy[j,i]= percentile(Qbes_block[j,:],50)
        Qbes_max[j,i]= percentile(Qbes_block[j,:],75)

        ϵ̇_min[j,i]= percentile(ϵ̇_block[j,:],25)
        ϵ̇_moy[j,i]= percentile(ϵ̇_block[j,:],50)
        ϵ̇_max[j,i]= percentile(ϵ̇_block[j,:],75)
    end

    try
        @load "$(fp_load)$(run_name[end])_6_outputs_fixed_para_100yrs.jld2"  pg
        global pg=pg
    catch
        @load "$(fp_load)$(run_name[end])_6_outputs_fixed_para_paracombo_debris_100yrs.jld2"  pg
        global pg=pg
    end
    
    index=[]
    for i =1:size(ϵ̇_block,1) 
        if zs_block[i] -zb_block[i] >5
            push!(index,i)
        end
    end
    if run_name[i] == "debris"
        push!(Qbs_catch_d, maximum(abs.(Qbs_block.*yr),dims=1)')    
        push!(  ϵ̇_catch_d, yr*1000*pg.ds*sum( ϵ̇_block[index,:]         ,dims=1))
        push!( ṁt_catch_d, yr*1000*pg.ds*sum(ṁt_block[index[1:end-1],:],dims=1))
        push!(Dm_d,Dm_block)
        push!(Kg_d,Kg_block)
        push!(l_er_d,l_er_block)  
        push!(hts_bulb_d,mean(hts_block[95:100,:],dims=1))

        push!(  ϵ̇_rate_d, 1e3*yr*mean( ϵ̇_block[index,:]         ,dims=1))
        push!( ṁt_rate_d, 1e3*yr*mean(ṁt_block[index[1:end-1],:],dims=1))
        push!(Qbs_rate_d, 1e3*Qbs_catch_d./(pg.ds*1000*length(index)))

    else
        push!(Qbs_catch_c, maximum(abs.(Qbs_block.*yr),dims=1)')    
        push!(  ϵ̇_catch_c, yr*1000*pg.ds*sum( ϵ̇_block[index,:]         ,dims=1))
        push!( ṁt_catch_c, yr*1000*pg.ds*sum(ṁt_block[index[1:end-1],:],dims=1))
        push!(Dm_c,Dm_block)
        push!(Kg_c,Kg_block)
        push!(l_er_c,l_er_block)  
        
        push!(  ϵ̇_rate_c, 1e3*yr*mean( ϵ̇_block[index,:]         ,dims=1))
        push!( ṁt_rate_c, 1e3*yr*mean(ṁt_block[index[1:end-1],:],dims=1))
        push!(Qbs_rate_c, 1e3*Qbs_catch_c./(pg.ds*1000*length(index)))
    end
    

end

Qbs_catch_d = hcat(Qbs_catch_d...);ϵ̇_catch_d = hcat(ϵ̇_catch_d...); ṁt_catch_d = hcat(ṁt_catch_d...);
Qbs_rate_d  = hcat(Qbs_rate_d...);
Qbs_rate_d  = hcat(Qbs_rate_d...);
ϵ̇_rate_d = hcat(ϵ̇_rate_d...);  ṁt_rate_d  = hcat(ṁt_rate_d...); Dm_d=hcat(Dm_d...);Kg_d=hcat(Kg_d...); l_er_d=hcat(l_er_d...)
hts_bulb_d=hcat(hts_bulb_d...)

Qbs_catch_c = hcat(Qbs_catch_c...);ϵ̇_catch_c = hcat(ϵ̇_catch_c...); ṁt_catch_c = hcat(ṁt_catch_c...);Dm_c=hcat(Dm_c...);Kg_c=hcat(Kg_c...); l_er_c=hcat(l_er_c...)
Qbs_rate_c  = hcat(Qbs_rate_c...);
Qbs_rate_c  = hcat(Qbs_rate_c...); ϵ̇_rate_c = hcat(ϵ̇_rate_c...) ;  ṁt_rate_c = hcat(ṁt_rate_c...)




hts_mean=hcat(hts_mean...);Qbs_mean=hcat(Qbs_mean...); ϵ̇_mean=hcat(ϵ̇_mean...); ṁt_mean=hcat(ṁt_mean...)
H20_velocity=hcat(H20_velocity...); 
