using Parameters
using PyPlot
import Roots: find_zero
using Dates
using JLD2
using StatsBase
using DelimitedFiles

#################
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
    ice_data_loc="/Users/Ian/data/debris/"
end

yr= 365*86400
file_name="dm_test_005"
#file_name="dm_test_01"
#file_name="dm_test_03"
file_name="5deg_hw_0.05"



# load erosion data
@load "$(fp_load)debris_$(file_name)_outputs.jld2" hts Qw Qbs zs_ zb_ ϵ̇ ṁt velocity  ∂ϕ_∂x spins  pg pp pn
hts_debris=hts;  Qw_debris=Qw; Qbs_debris=Qbs; zs_debris= zs_;zb_debris= zb_;ϵ̇_debris=ϵ̇;ṁt_debris= ṁt ; H2O_velocity_debris=velocity;  ∂ϕ_∂x_debris= ∂ϕ_∂x; spins_debris=spins;  pg_debris=pg; pp_debris=pp; pn_debris=pn

for i = 1:length(pg.sgrid)-1
    if ṁt[i] == 0.0
        Qw_debris[i] =NaN; Qbs_debris[i] =NaN; ϵ̇[i]=NaN; ṁt_debris[i] =NaN; hts_debris[i]=NaN;  H2O_velocity_debris[i] = NaN        
    end
end



@load "$(fp_load)clean_$(file_name)_outputs.jld2" hts Qw Qbs zs_ zb_ ϵ̇ ṁt velocity  ∂ϕ_∂x spins  pg pp pn
hts_clean=hts;  Qw_clean=Qw; Qbs_clean=Qbs; zs_clean= zs_;zb_clean= zb_;ϵ̇_clean=ϵ̇;ṁt_clean= ṁt ; H2O_velocity_clean=velocity;  ∂ϕ_∂x_clean= ∂ϕ_∂x; spins_clean=spins;  pg_clean=pg; pp_clean=pp; pn_clean=pn

for i = 1:length(pg.sgrid)-1
    if ṁt[i] == 0.0
        Qw_clean[i] =NaN; Qbs_clean[i] =NaN; ϵ̇[i]=NaN; ṁt_clean[i] =NaN; hts_clean[i]=NaN; H2O_velocity_clean[i] = NaN
    end
end



# load ice data
ice_data_debris = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian.csv", ',',skipstart=1)
ice_data_clean = DelimitedFiles.readdlm("$(ice_data_loc)debris_free_example_for_ian.csv", ',',skipstart=1)

################
### plotting ###
################
figure(2,figsize=(10,10)); clf()
subplot(4,1,1)
# p1=fill_between(ice_data_debris[1:160,1], reverse(ice_data_debris[1:160,8]),0, color="grey")
# ylabel("Debris H (m)", color="grey")
# xlim([pg.sgrid[1], pg.sgrid[end]])
# xticks([])


p1b = twinx()
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), hts_debris, color="blue")
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), hts_clean,  color="orange")
ylabel("till (m)")
xticks([])
title("$(file_name)")

p2=subplot(4,1,2)
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), ṁt_debris.*yr*1e3, color="blue",  label="debris")
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), ṁt_clean .*yr*1e3, color="orange",label="clean")
ylabel("ṁt (mm a⁻¹)")
xticks([])
legend()

p2b = twinx()
plot(pg.sgrid, ϵ̇_debris.*yr*1e3, color="blue",   ls=":",label="debris")
plot(pg.sgrid, ϵ̇_clean .*yr*1e3, color="orange", ls=":",label="clean")

ylabel("ϵ̇ (mm a⁻¹)")
xticks([])

subplot(4,1,3, sharex=p2)
yticks([])
p1b = twinx()
plot(pg.sgrid,abs.(Qw_debris), color="blue")
plot(pg.sgrid,abs.(Qw_clean), color="orange")
#ylabel("Qs (m³ s⁻¹)")
legend()
xticks([])



subplot(4,1,4, sharex=p2)
plot(ice_data_debris[1:160,1], reverse(ice_data_debris[1:160,4]),0, color="blue",  ls=":",label=L"u$_b$")
plot(ice_data_clean[1:160,1],  reverse(ice_data_clean[1:160,4] ),0, color="orange",ls=":")
ylabel("Ub (m a⁻¹)", color="black")
xlim([pg.sgrid[1], pg.sgrid[end]])
xticks([])
legend()
 
p4=twinx()

p1=plot(pg.sgrid,abs.(H2O_velocity_debris), color="blue",label="water velo")
plot(   pg.sgrid,abs.(H2O_velocity_clean),  color="orange")
ylabel(" H₂O vel. (m s⁻¹)")
xticks(range(pg.sgrid[1],stop=pg.sgrid[end]+50,length=5))
legend()

tight_layout()
subplots_adjust(hspace=0.0)

savefig("$(fp_save)model_outputs.pdf")



tmp_clean  = maximum(findall(x->isnan(x),ϵ̇_clean ))+3
tmp_debris = maximum(findall(x->isnan(x),ϵ̇_debris))+3
glac_area_clean = 1000*(abs(pg.sgrid[end]-pg.sgrid[tmp_clean]))
glac_area_debris = 1000*(abs(pg.sgrid[end]-pg.sgrid[tmp_debris]))

println("
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
        Clean glacier
    
        Erosion rate: $(round(mean(ϵ̇_clean[tmp_clean:end]) *yr*1e3,digits=4)) mm a⁻¹ no bedrock limitation
       
        Till production:  $(round(mean(ṁt_clean[tmp_clean:end])*yr*glac_area_clean,digits=4)) m³ a⁻¹ 
            area-scaled:  $(round(mean(ṁt_clean[tmp_clean:end])*yr*1e3,digits=4)) mm a⁻¹
       
     Sediment discharge: $(round(abs(Qbs_clean[tmp_clean]),digits=4)) m³ s⁻¹

=======================================================================

        Debris glacier

        Erosion rate: $(round(mean(ϵ̇_debris[tmp_debris:end]) *yr*1e3,digits=4)) mm a⁻¹ no bedrock limitation
       
        Till production:  $(round(mean(ṁt_debris[tmp_debris:end])*yr*glac_area_debris,digits=4)) m³ a⁻¹ 
            area-scaled:  $(round(mean(ṁt_debris[tmp_debris:end])*yr*1e3,digits=4)) mm a⁻¹
       
     Sediment discharge: $(round(abs(Qbs_debris[tmp_debris]),digits=4)) m³ s⁻¹
    
    ")
