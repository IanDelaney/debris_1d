# a script to search for different parameters.

using Distributed

if length(procs())==1
    addprocs(2) 
end

@everywhere begin
    using Pkg
    Pkg.activate(".")
    
    using Parameters
    using Dierckx
    using DifferentialEquations
    import Roots: find_zero
    using Dates
    using JLD2
    using DelimitedFiles
    using SUGSET
    using SUGSET: erosion, post_proc
    import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,spinner, Num, Phys
    include("interface-ice.jl")
    

end

@eval @everywhere begin
    lower=$lower
    upper=$upper
    #Mountain=$Mountain
    pg=$pg
    ice_data_loc = $ice_data_loc
    run_name = $run_name
    parameter_combo=$parameter_combo
end
@everywhere begin
    include("interface-ice.jl")
    include("file_load.jl")
    
    pg=Mountain(pg,
                sgrid=range(pg.domain[1],stop=pg.domain[2],length=350),
                )
    const total_runs = 100
    const Upper = upper
    const Lower = lower
    no_trys_runs = 5000

    if run_name == "clean"
        if parameter_combo == "clean"
        # l_er_l=[];Kg_l=[k]; Dm_l=[];ϵ̇_l=[]
        
        
        # v1 = ((Upper[1].-Lower[1])*rand(MersenneTwister(123),      no_trys_runs).+Lower[1]) # i
        # v2 = (rand(MersenneTwister(456),Uniform(Lower[2],Upper[2]),no_trys_runs))           # j
        # v3 = ((Upper[3].-Lower[3])*rand(MersenneTwister(789),      no_trys_runs).+Lower[3]) # k
      
        #     for i= 1:no_trys_runs
                
        #         pp= Phys(
        #             Kg   = v2[i],
        #             l_er = v3[i])
                
        #         ϵ̇_ =[]
        #         for i=1:length(pg.sgrid)
        #             tmp= SUGSET.source_till(pg.sgrid[i], 0, pg, pp)*year
        #             if tmp > 0
        #                 push!(ϵ̇_,tmp)
        #             end
        #         end
        #         ϵ̇=mean(ϵ̇_)
                
        #         if ϵ̇ > 1e-3 && ϵ̇ <20e-3 # Non-polar rates from Hallet 1996
        #             push!(Dm_l,v1[i]);push!(Kg_l,v2[i]);push!(l_er_l,v3[i]);push!(ϵ̇_l,ϵ̇)
        #             #println(ϵ̇)
        #         end
        #     end
            
        #     Dm_l=hcat(Dm_l...); Kg_l=hcat(Kg_l...); l_er_l =hcat(l_er_l...);ϵ̇_l =hcat(ϵ̇_l...)
            
        #     @save "Parameters_clean.jld2" Dm_l Kg_l l_er_l ϵ̇_l
            @load "Parameters_clean.jld2" Dm_l Kg_l l_er_l

        else
            @load "Parameters_debris.jld2" Dm_l Kg_l l_er_l
        end
        
       
        const v_1 = Dm_l# i
        const v_2 = Kg_l # j
        const v_3 = l_er_l# k
        

    else
        if parameter_combo== "debris"
         
        # l_er_l=[];Kg_l=[]; Dm_l=[];ϵ̇_l=[]
        
        
        # v1 = ((Upper[1].-Lower[1])*rand(MersenneTwister(123+10),      no_trys_runs).+Lower[1]) # i
        # v2 = (rand(MersenneTwister(456+10),Uniform(Lower[2],Upper[2]),no_trys_runs))           # j
        # v3 = ((Upper[3].-Lower[3])*rand(MersenneTwister(789+10),      no_trys_runs).+Lower[3]) # k

        #     for i= 1:no_trys_runs
                
        #         pp= Phys(
        #             Kg   = v2[i],
        #             l_er = v3[i])
                
        #         ϵ̇_ =[]
        #         for i=1:length(pg.sgrid)
        #             tmp= SUGSET.source_till(pg.sgrid[i], 0, pg, pp)*year
        #             if tmp > 0
        #                 push!(ϵ̇_,tmp)
        #             end
        #         end
        #         ϵ̇=mean(ϵ̇_)
                
        #         if ϵ̇ > 1e-3 && ϵ̇ <20e-3 # Non-polar rates from Hallet 1996
        #             push!(Dm_l,v1[i]);push!(Kg_l,v2[i]);push!(l_er_l,v3[i]);push!(ϵ̇_l,ϵ̇)
        #             #println(ϵ̇)
        #         end
        #     end
            
        #     Dm_l=hcat(Dm_l...); Kg_l=hcat(Kg_l...); l_er_l =hcat(l_er_l...);ϵ̇_l =hcat(ϵ̇_l...)
            
        #     @save "Parameters_debris.jld2" Dm_l Kg_l l_er_l ϵ̇_l
            @load "Parameters_debris.jld2" Dm_l Kg_l l_er_l

        else
            @load "Parameters_clean.jld2" Dm_l Kg_l l_er_l
        end
    
        const v_1 = Dm_l# i
        const v_2 = Kg_l # j
        const v_3 = l_er_l# k
        
    end
    
end

function parameter_search_ice(pp, pg, pn, fp_save,spin_threshold,break_time,run_name,parameter_combo) 
    pmap(nr->one_evaluation_debris(nr, pg, pp, pn, fp_save,spin_threshold,break_time,run_name,parameter_combo), 1:total_runs)
    return nothing
end


@everywhere begin
    
    function one_evaluation_debris(nr,pg,pp,pn,fp_save,spin_threshold,break_time,run_name,parameter_combo)
        println("Running simulation $nr of $(total_runs)")

        pp= Phys(pp,
                 Dm   = v_1[nr],
                 Kg   = v_2[nr],
                 l_er = v_3[nr])

        ϵ̇_ =[]
        for i=1:length(pg.sgrid)
            tmp= SUGSET.source_till(pg.sgrid[i], 0, pg, pp)*year
            if tmp > 0
                push!(ϵ̇_,tmp)
            end
        end
        ϵ̇=mean(ϵ̇_)
        
        #  if ϵ̇ > 1e-3 && ϵ̇ <20e-3 # Non-polar rates from Hallet 1996
        
        println("Adequate Parameter Combination $(parameter_combo) for $(run_name), Run: $(nr), Erosion rate: $(ϵ̇*1e3) mm a⁻¹")
        
        hts, Qbs, spins  =   one_eval(pg,pp,pn,spin_threshold,break_time);
        
        Qw  = -SUGSET.Q_w(pg.sgrid, 0.0, pg, pp)
        zs_ = SUGSET.zs.(pg.sgrid, Ref(0.0) , Ref(pg))
        zb_ = SUGSET.zb.(pg.sgrid, Ref(0.0) , Ref(pg))
        ϵ̇   = SUGSET.source_till.(pg.sgrid, Ref(0.0), Ref(pg), Ref(pp))
        ub  = SUGSET.basal_velocity.(pg.sgrid, Ref(0.0), Ref(pg),Ref(pp))
        ṁt  =  SUGSET.effective_sed_source.(pg.sgrid[1:end-1], Ref(0.0), Ref(pg), Ref(pp), Ref(pn), hts )
        velocity  = Qw  ./ SUGSET.xsect(pg.sgrid, 0.0, pg, pp, pn)[1]
        ∂ϕ_∂x = SUGSET.Ψˣ.(pg.sgrid,Ref(0.0),Ref(pg),Ref(pp))
        surf_debris = surf_deb_t.(pg.sgrid, Ref(0.0), Ref(pg))
        Qbe=-SUGSET.sed_transport(pg.sgrid,0.0, pg, pp, pn)[1]
        

        
        @save "$(fp_save)$(run_name)_$(nr)_outputs_fixed_para_paracombo_$(parameter_combo)_$(break_time)yrs.jld2" hts Qw Qbs zs_ zb_ ϵ̇ ṁt velocity  ∂ϕ_∂x ub Qbe spins  pg pp pn

        
        return nothing
        # else
        #     return nothing
        # end
        
        #end
    end
end
