function return_run(fp_load,run_name)
    hts_block=[];Qbs_block=[];Qbes_block=[];ϵ̇_block=[]; ṁt_block=[]; Dm_block =[]; Kg_block=[];l_er_block=[];
    
    for i= 1:100
        #file_name="parameter_search_$(i)"
        # load erosion data
        #try
                               
        @load "$(fp_load)$(run_name)_$(i)_outputs_fixed_para_paracombo_debris_100yrs.jld2" hts Qw Qbs Qbe zs_ zb_ ϵ̇ ṁt velocity  ∂ϕ_∂x spins  pg pp pn
        println("$(run_name)...$(i)")

        push!(hts_block,hts);  Qw_block=Qw; push!(Qbs_block,Qbs);push!(Qbes_block,Qbe); push!(ϵ̇_block,ϵ̇);push!(ṁt_block,ṁt) ; push!(Dm_block,pp.Dm);push!(Kg_block,pp.Kg);push!(l_er_block,pp.l_er)

        @load "$(fp_load)$(run_name)_$(i)_outputs_fixed_para_paracombo_clean_100yrs.jld2" hts Qw Qbs Qbe zs_ zb_ ϵ̇ ṁt velocity  ∂ϕ_∂x spins  pg pp pn

        push!(hts_block,hts);  Qw_block=Qw; push!(Qbs_block,Qbs);push!(Qbes_block,Qbe); push!(ϵ̇_block,ϵ̇);push!(ṁt_block,ṁt) ; push!(Dm_block,pp.Dm);push!(Kg_block,pp.Kg);push!(l_er_block,pp.l_er)

        
        if i==1
            global zs_block= zs_;
            global zb_block= zb_;
            global H2O_velocity_block=velocity;
            global ∂ϕ_∂x_block= ∂ϕ_∂x;
            # catch
        end
    end
    
    hts_block=hcat(hts_block...);Qbs_block=hcat(Qbs_block...);Qbes_block=hcat(Qbes_block...);ϵ̇_block=hcat(ϵ̇_block...);ṁt_block=hcat(ṁt_block...)
    
    return hts_block, Qbs_block, -Qbes_block, ϵ̇_block, ṁt_block, Dm_block, zs_block, zb_block,H2O_velocity_block, ∂ϕ_∂x_block, Dm_block, Kg_block, l_er_block
end
