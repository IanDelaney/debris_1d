using Parameters
using PyPlot
import Roots: find_zero
using Dates
using JLD2
using StatsBase
using DelimitedFiles
using HypothesisTests
include("post_proc_funs.jl")
#################"
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
end

yr= 365*86400
test= @isdefined Qbs_catch_d
   run_name=["debris" "clean"]
if test == false    
    include("ensemble_load.jl")
    @load "$(fp_load)model_setup.jld2" sgrid zb_clean zb_debris zs_clean zs_debris ṁw_debris ṁw_clean Qw_clean Qw_debris ∂ϕ_∂x_debris ∂ϕ_∂x_clean ub_debris ub_clean surf_debris
end
# # # load ice data

##################
smid  = pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1])
sgrid = pg.sgrid


figure(2,figsize=(9.8,7.5)); clf()
#subplot(3,1,1)
subplot2grid((9,3), (0,0), rowspan=2, colspan=3)

axvspan(9800,12340 ,color="grey", alpha=.5)#
ax1=fill_between(sgrid,surf_debris, 0, color="sienna")
ylabel("Debris \n thickness \n (m)", color="sienna")
ylim([0.001, 2.7] )
xlim([sgrid[1],16000])
xticks([])

p2b=twinx()

for i =length(run_name):-1:1
    if i==1
        plot(smid[1:end-9], reverse(hts_moy[10:end,i]),label=run_name[i],color="darkorange")
        fill_between(smid[1:end-9], reverse(hts_min[10:end,i]), reverse(hts_max[10:end,i]),color="darkorange",alpha=.5)
    else
        plot(smid[1:end-111],reverse( hts_moy[112:end,i]),label=run_name[i], color="dodgerblue")
        fill_between(smid[1:end-111], reverse(hts_min[112:end,i]), reverse(hts_max[112:end,i]), color="dodgerblue",alpha=.5)
    end
end
fill_between(-sgrid[1:2],[10^-15, 10^-14], [10^-15,10^-14], color ="sienna", alpha=.75, label="Debris Thickness")
text(400,.22,"a")
text(12000,.02,"  Bulb location",rotation=270)

legend()
ylim([0.01, .25])
xlim([sgrid[1],16000])
ylabel("Till Height \n (m)", rotation=270, labelpad=30)
xticks([])





subplot2grid((9,3), (2,0), rowspan=2, colspan=3)
# for i =length(run_name):-1:1 
#     if i==1
#         plot(smid[1:end-9]', reverse(ϵ̇_moy[11:end,i] .- ṁt_moy[10:end,i] )*yr*1e3,color="darkorange")
#         fill_between(smid[1:end-9], reverse(ϵ̇_min[11:end,i] .-ṁt_min[10:end,i] )*yr*1e3, reverse(ϵ̇_max[11:end,i] .- ṁt_max[10:end,i])*yr*1e3,color="darkorange",alpha=.5)
#     else
#         plot(smid[1:end-109], reverse(ϵ̇_moy[111:end,i]    .- ṁt_moy[110:end,i])*yr*1e3, color="dodgerblue")
#         fill_between(smid[1:end-109], reverse(ϵ̇_min[111:end,i] .- ṁt_min[110:end,i])*yr*1e3, reverse(ϵ̇_max[111:end,i] .- ṁt_max[110:end,i])*yr*1e3, color="dodgerblue",alpha=.5)
#     end
# end
# axvspan(9800,12340 ,color="grey", alpha=.5)#
# text(400,9,"b")
# text( 1300, 7, "Glacier flow direction")
# arrow(1500, 6,2000,0,width=.75,head_length=500,color="black")
# text(12000, 1.1, "Glacier terminus")
# arrow(12000,1,smid[end-109]-11700,-0.35,head_length=200, head_width=.125, color="black") 
# arrow(14300,1,smid[end-9]-14600,-0.3,head_length=200, head_width=.125, color="black")
# xlim([sgrid[1],16000])
# ylim([.01, 11.5])
# ylabel("Erosion - Till Source \n (mm a⁻¹)")
# xticks([])

# p2b=twinx()
# for i= length(run_name):-1:1
#     if i==1
#         p1=plot(sgrid[1:end-9],reverse(abs.(H20_velocity[10:end,i])),color="darkorange", linewidth=3)
#     else
#         p1=plot(sgrid[1:end-109],reverse(abs.(H20_velocity[110:end,i])), color="dodgerblue", linewidth=3)
#     end
# end


# ylabel(" H₂O \n Velocity \n (m s⁻¹)", rotation=270, labelpad=35)
# ylim([.01, 1.1])
# xticks([])
p2bd=twinx()

for i= length(run_name):-1:1
    if i==1
        p1=semilogy(sgrid[1:end-10],diff(reverse(abs.(Qbes_moy[10:end,i]).*yr)),color="darkorange", linewidth=3,":")

        #fill_between(sgrid[1:end-9], reverse(Qbes_min[10:end,i])*yr,reverse(Qbes_max[10:end,i])*yr,color="darkorange",alpha=.5)
    else
        p1=semilogy(sgrid[1:end-110],diff(reverse(abs.(Qbes_moy[110:end,i]).*yr)), color="dodgerblue", linewidth=3,":")
        #fill_between(sgrid[1:end-109], reverse(Qbes_min[110:end,i])*yr,reverse(Qbes_max[110:end,i])*yr,color="dodgerblue",alpha=.5)
    end
end


ylabel(" Sediment transport \n capacity \n (m³ a⁻¹)", rotation=270, labelpad=35)
ylim([1e1, 1e5])
xticks([])

subplot2grid((9,3), (4,0), rowspan=2, colspan=3)


axvspan(9800,12340 ,color="grey", alpha=.5)#
for i =length(run_name):-1:1
    if i==1

        plot(sgrid[1:4:end-10],  ∂ϕ_∂x_debris[1:4:end-10],color="darkorange","--")

    else
        plot(sgrid[1:4:end-120], ∂ϕ_∂x_clean[1:4:end-120], color="dodgerblue", "--")
    end
end
text(400,2100,"c")
xlim([sgrid[1],16000])
xticks([])
ylabel("Hydro. \n Potential Grad. \n (Pa m⁻¹)")
xlabel("x (m)")

p1b = twinx()

for i =length(run_name):-1:1
    if i==1
        plot(sgrid[1:end-11],  diff(∂ϕ_∂x_debris[1:end-10])./pg.ds*4,color="darkorange")
    else
        plot(sgrid[1:end-121], diff(∂ϕ_∂x_clean[1:end-120])./pg.ds*4, color="dodgerblue")
    end
end
#ylim([10, 69000])
axhspan(0, -2, 0.0, 17000,color="lightgrey")
xlim([sgrid[1],16000])
ylim([-.063, .79])
xticks([])
ylabel("Hydro.  Potential \n Curvature \n (Pa m⁻2)", rotation=270, labelpad=35)
xlabel("x (m)")

plot([10^-15, 10^-14], [10^-15,10^-14], color ="black",      label="Curvature Hydro. Potential")
plot([10^-15, 10^-14], [10^-15,10^-14], color ="black","--", lw=3, label="Hydro. Potential")
legend(loc=1)

subplot2grid((9,3), (6,0), rowspan=2, colspan=3)
axvspan(9800,12340 ,color="grey", alpha=.5)#
plot([0, 1], [0,1], color ="black",":", label=L" $\mathrm{Q_w}$")
plot([0, 1], [0,1], color ="black", label=L"$\dot{m}_w$")
text(400, 3.25, "d")

plot(sgrid[1:end-13+1], reverse(abs.(Qw_debris[13:end])),color="darkorange", ":")
plot(sgrid[1:236], reverse(abs.(Qw_clean[115:end])), color="dodgerblue", ":")
ylim([.01, 3.9])
xlim([sgrid[1],16000])
legend(loc=6)
ylabel("Water discharge \n (m³  s⁻¹)")

axb = twinx()
ax1=plot(sgrid[1:end-14], ṁw_debris[1:end-14]*yr,color="darkorange")
plot(    sgrid[1:end-116], ṁw_clean[1:end-116]*yr, color="dodgerblue")
ylim([0.5, 29.9])
xlim([sgrid[1],16000])
#xticks([])
ylabel("Melt rate \n (m a⁻¹)",rotation=270, labelpad=30)
xticks([0,  4000, 8000 , 12000 , 16000])




subplots_adjust(hspace=0)
savefig("$(fp_save)model_process.pdf")
println("
                    ==============================================================

                            $(run_name[1]) glacier

                            Erosion rate (mean): $(round(mean(ϵ̇_rate_d),digits=5)) mm a⁻¹ no bedrock limitation
                            Erosion rate (mean): $(round(mean(ϵ̇_catch_d),digits=5)) m³ a⁻¹ no bedrock limitation
                           
                            Till production (mean): $(round(mean(ṁt_catch_d),digits=5)) m³ a⁻¹ 
                                area-scaled (mean): $(round(mean(ṁt_rate_d), digits=5)) mm a⁻¹
                           
                         Sediment discharge (mean): $(round(mean(Qbs_catch_d),digits=5)) m³ a⁻¹
                                area-scaled (mean): $(round(mean(Qbs_rate_d) ,digits=5)) mm a⁻¹

                    ==============================================================

                            $(run_name[2]) glacier

                            Erosion rate (mean): $(round(mean(ϵ̇_rate_c),digits=5)) mm a⁻¹ no bedrock limitation
                            Erosion rate (mean): $(round(mean(ϵ̇_catch_c),digits=5)) m³ a⁻¹ no bedrock limitation
                           
                            Till production (mean): $(round(mean(ṁt_catch_c),digits=5)) m³ a⁻¹ 
                                area-scaled (mean): $(round(mean(ṁt_rate_c), digits=5)) mm a⁻¹
                           
                         Sediment discharge (mean): $(round(mean(Qbs_catch_c),digits=5)) m³ a⁻¹
                                area-scaled (mean): $(round(mean(Qbs_rate_c) ,digits=5)) mm a⁻¹

                    ==============================================================
                            
                            Two sample T-Test

                            Erosion rate: 

$(OneSampleZTest(ϵ̇_rate_c[:],ϵ̇_rate_d[:]))

                            Erosion quantity: 

$(OneSampleZTest(ϵ̇_catch_c[:],ϵ̇_catch_d[:]))

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
                            Till production:

 $(OneSampleZTest(ṁt_catch_c[:],ṁt_catch_d[:]))
                                
                                area-scaled: 

$(OneSampleZTest(ṁt_rate_c[:],ṁt_rate_d[:]))


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


                         Sediment discharge: 

$(OneSampleZTest(Qbs_catch_c[:], Qbs_catch_d[:]))
                                

                        area-scaled: 

$(OneSampleZTest(Qbs_rate_c[:] , Qbs_rate_d[:]))
                        

Note for below: Erosion (area-scaled and catch) of low erosion and high erosion has pvalue     
     outcome with 95% confidence: reject h_0
      two-sided p-value:           <1e-15
SEE HISTOGRAM Comments below



")
# figure()
# for i =1:length(run_name)
#     plot(sum(ṁt_mean[:,i],dims=1)*1000*maximum(diff(pg.sgrid))*yr, maximum(-Qbs_mean[:,i],dims=1)*yr, "*",label=run_name[i])

# end
# xlabel("ṁt (m³ a⁻¹")
# ylabel("Qₛ (m³ a⁻¹")
# legend()

# # shows that it depends on sediment transport, not necessarily erosion.... not characteristic of high erosion rates. 



dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )
        
       
    else
        push!(dep,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )

    end
end

trans= vcat(trans...)

dep =vcat(dep...)







println("

\\begin{table}
 \\caption{Model outputs}
  \\centering
  \\begin{tabular}{l c c c  |c c c }
    \\hline
                        & \\cl & \\db &  p & \\db & \\db & p \\\\
                        &     &       &    & bulb & no bulb & \\\\  \\hline
    n                     &$(length(ṁt_rate_c))  & $(length(ṁt_rate_d))  &  & $(length(dep[:,1]))& $(length(trans[:,1])) &  \\\\ 
                          &      &      &    &&& \\\\
 
  \$ \\dot{\\epsilon} \$ (\\mma) &$(round(mean(ϵ̇_rate_c),digits=2)) & $(round(mean(ϵ̇_rate_d),digits=2))  &  $(round(pvalue(MannWhitneyUTest(ϵ̇_rate_c[:],ϵ̇_rate_d[:])),digits=3))

&$(round(mean(dep[:,1]),digits=2))  & $(round(mean(trans[:,1]),digits=2)) &  \\textbf{$(round(pvalue(MannWhitneyUTest(trans[:,1],dep[:,1])),digits=3)) }

 \\\\
  \$ \\dot{m}_t \$  (\\mma)     &$(round(mean(ṁt_rate_c),digits=2))& $(round(mean(ṁt_rate_d),digits=2))  & \\textbf{ $(round(pvalue(MannWhitneyUTest(ṁt_rate_c[:],ṁt_rate_d[:])),digits=3)) }

&$(round(mean(dep[:,2]),digits=2))  & $(round(mean(trans[:,2]),digits=2)) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,2],dep[:,2])),digits=3)) }
   \\\\
  \$ \\dot{Q_s} \$ (\\mma)      &$(round(mean(Qbs_rate_c),digits=2))&$(round(mean(Qbs_rate_d),digits=2)) & \\textbf{ $(round(pvalue(MannWhitneyUTest(Qbs_rate_c[:],Qbs_rate_d[:])),digits=3))}

&$(round(mean(dep[:,3]),digits=2))  & $(round(mean(trans[:,3]),digits=2)) &  $(round(pvalue(MannWhitneyUTest(trans[:,3],dep[:,3])),digits=3))

  \\\\
                          &      &      &      &&\\\\
  \$ \\epsilon \$ (\\mmma)      &$(round(mean(ϵ̇_catch_c))) &$(round(mean(ϵ̇_catch_d))) & \\textbf{ $(round(pvalue(MannWhitneyUTest(ϵ̇_catch_c[:],ϵ̇_catch_d[:])),digits=3))}

&$(round(mean(dep[:,4])))  & $(round(mean(trans[:,4]))) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,4],dep[:,4])),digits=3)) }

    \\\\
  \$ m_t \$  (\\mmma)          &$(round(mean(ṁt_catch_c)))&$(round(mean(ṁt_catch_d))) &$(round(pvalue(MannWhitneyUTest(ṁt_catch_c[:],ṁt_catch_d[:])),digits=3))

&$(round(mean(dep[:,5])))  & $(round(mean(trans[:,5]))) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,5],dep[:,5])),digits=3))}

    \\\\
  \$ Q_s \$ (\\mmma)           &$(round(mean(Qbs_catch_c)))&$(round(mean(Qbs_catch_d)))& $(round(pvalue(MannWhitneyUTest(Qbs_catch_c[:],Qbs_catch_d[:])),digits=3))

&$(round(mean(dep[:,6])))  & $(round(mean(trans[:,6]))) &  $(round(pvalue(MannWhitneyUTest(trans[:,6],dep[:,6])),digits=3))

   \\\\

           &      &      &    &&& \\\\
 \$D_m \$ (\\cm) &      &      &    &$(round(mean(dep[:,7]),digits=4)*10) & $(round(mean(trans[:,7]),digits=4)*10) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,7],dep[:,7])),digits=3)) }\\\\    \\hline
           &      &      &    &&& \\\\

 \\multicolumn{7}{l}{Bold font represents significance at the \$97.5\$\\% confidence interval with OneSampleZTest.}

  \\end{tabular}
\\end{table}


")
