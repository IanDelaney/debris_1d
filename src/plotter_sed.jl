using Parameters
using PyPlot
import Roots: find_zero
using Dates
using JLD2
using StatsBase
using DelimitedFiles
using HypothesisTests
include("post_proc_funs.jl")
#################"
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
end
%
yr= 365*86400
test= @isdefined Qbs_catch_d
   run_name=["debris" "clean"]
# # # load ice data
if test == false 
    
    include("ensemble_load.jl")
    
    @load "$(fp_load)model_setup.jld2" sgrid zb_clean zb_debris zs_clean zs_debris ṁw_debris ṁw_clean Qw_clean Qw_debris ∂ϕ_∂x_debris ∂ϕ_∂x_clean ub_debris ub_clean surf_debris
    
    smid = range(sgrid.start,sgrid.stop,length=sgrid.lendiv)
    sgrid = range(sgrid.start,sgrid.stop,length=sgrid.len)

end

#sgrid=reverse(sgrid)

##################


figure(2,figsize=(9.8,7.5)); clf()
#subplot(3,1,1)
subplot2grid((9,3), (0,0), rowspan=2, colspan=3)


fill_between(-sgrid[1:2],[10^-15, 10^-14], [10^-15,10^-14], color ="black", alpha=.75, label="Ice Thickness")
plot([10^-15, 10^-14], [10^-15,10^-14], color ="black", label="Basal Velocity")
legend()

fill_between(   sgrid,0.0 .* zs_debris,zs_debris .- zb_debris, color="darkorange",label= "debris",alpha=.75)
p1=fill_between(sgrid,0.0 .* zs_clean,zs_clean .- zb_clean, color="dodgerblue",  label= "clean",alpha=.75)
text(sgrid[17], 1.01*maximum(zs_clean), "a")
#ylim([minimum(zs_debris),maximum(zs_debris)*1.05])
xlim([sgrid[1],16000])
ylim([2.5, maximum(zs_clean .- zb_clean)*1.25])
axvspan(9800,12340 ,color="grey", alpha=.5)#
xticks([])

ylabel("Ice thickness \n (m)")
xticks([])

p1b = twinx()

p1=plot(sgrid,ub_clean*yr, color="dodgerblue",  label= L"CLEAN")
plot(sgrid,ub_debris*yr, color="darkorange",  label= L"DEBRIS")
text(sgrid[17], 1.01*maximum(zs_clean), "a")
ylim([0.2,6.75])
xlim([sgrid[1],16000])
text(reverse(sgrid)[96]*1.01,1,"  Bulb location \n     (see text)",rotation=270)

xticks([])

ylabel("Basal velocity \n (m a⁻¹)",rotation=270, labelpad=30)
xticks([])

text(400,5,"a")

subplot2grid((9,3), (2,0), rowspan=2, colspan=3)

axvspan(9800,12340 ,color="grey", alpha=.5)#
axhline(mean(pg.ht0),color="black",linewidth=.5,ls=":")
text(13000,mean(pg.ht0), "initial condition")

for i =length(run_name):-1:1
    if i==1
        plot(smid[1:end-9], reverse(hts_moy[10:end,i]),label=run_name[i],color="darkorange")
        fill_between(smid[1:end-9], reverse(hts_min[10:end,i]), reverse(hts_max[10:end,i]),color="darkorange",alpha=.5)
    else
        plot(smid[1:end-116],reverse( hts_moy[117:end,i]),label=run_name[i], color="dodgerblue")
        fill_between(smid[1:end-116], reverse(hts_min[117:end,i]), reverse(hts_max[117:end,i]), color="dodgerblue",alpha=.5)
    end
end

text(400,.2,"b")


legend()
ylim([0.001, .27])
xlim([sgrid[1],16000])
ylabel("Till Height \n (m)")
xticks([])


subplot2grid((9,3), (4,0), rowspan=2, colspan=3)
axvspan(9800,12340 ,color="grey", alpha=.5)#

plot([10^-15, 10^-14], [10^-15,10^-14], color ="black",      label="Erosion rate with till effects")
plot([10^-15, 10^-14], [10^-15,10^-14], color ="black","--", lw=3, label="Erosion rate without till effects")
legend(loc=1)

for i =length(run_name):-1:1 
    if i==1
        semilogy(smid[1:end-8], reverse(ϵ̇_moy[10:end,i])*yr*1e3,color="darkorange",ls="--",lw=3)
        
        semilogy(smid[1:end-9], reverse(ṁt_moy[10:end,i])*yr*1e3,color="darkorange")
        fill_between(smid[1:end-9], reverse(ṁt_min[10:end,i])*yr*1e3, reverse(ṁt_max[10:end,i])*yr*1e3,color="darkorange",alpha=.5)
 
    else
        semilogy(smid[1:end-108], reverse(ϵ̇_moy[110:end,i])*yr*1e3, color="dodgerblue",ls="--",lw=3)
        semilogy(smid[1:end-109], reverse(ṁt_moy[110:end,i])*yr*1e3, color="dodgerblue")
        fill_between(smid[1:end-109], reverse(ṁt_min[110:end,i])*yr*1e3, reverse(ṁt_max[110:end,i])*yr*1e3, color="dodgerblue",alpha=.5)

    end
end

text(400,10,"c")
text( 1300, 9, "Glacier flow direction")
arrow(1500, 7,2000,0,width=.75,head_length=500,color="black")
text(12000, 1.1, "Glacier terminus")
arrow(12000,1,smid[end-109]-11700,-0.35,head_length=200, head_width=.125, color="black") 
arrow(14300,1,smid[end-9]-14600,-0.3,head_length=200, head_width=.125, color="black") 


xlim([sgrid[1],16000])
ylim([.5, 1.05*maximum(ṁt_max)*yr*1e3])
ylabel("Erosion Rate \n  (mm a⁻¹)")
xticks([])


subplot2grid((9,3), (6,0), rowspan=2, colspan=3)
axvspan(9800,12340 ,color="grey", alpha=.5)
for i =length(run_name):-1:1
    if i==1
        plot(sgrid[1:end-9], reverse(-Qbs_moy[10:end,i]*yr),color="darkorange")
        fill_between(sgrid[1:end-9],reverse( -Qbs_min[10:end,i]*yr),reverse( -Qbs_max[10:end,i]*yr),color="darkorange",alpha=.5)
    else
        plot(sgrid[1:end-109], reverse(-Qbs_moy[110:end,i])*yr, color="dodgerblue")
        fill_between(sgrid[1:end-109], reverse(-Qbs_min[110:end,i]*yr),reverse( -Qbs_max[110:end,i]*yr), color="dodgerblue",alpha=.5)
        
    end
end
text(400,60000,"d")
ylim([10, 69000])
xlim([sgrid[1],16000])
xticks([0,  4000, 8000 , 12000 , 16000])
ylabel("Sediment Discharge \n (m³ a⁻¹)")
xlabel("x (m)")


#tight_layout()
#subplots_adjust(bottom=0.15)
subplots_adjust(hspace=0)

savefig("$(fp_save)model_outputs.pdf")



###############
dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,ϵ̇_rate_d[i])
       
    else
        push!(dep,ϵ̇_rate_d[i])
    end
end


dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,Dm_d[i])
       
    else
        push!(dep,Dm_d[i])
    end
end


println("
                    ==============================================================

                            $(run_name[1]) glacier

                            Erosion rate (mean): $(round(mean(ϵ̇_rate_d),digits=5)) mm a⁻¹ no bedrock limitation
                            Erosion rate (mean): $(round(mean(ϵ̇_catch_d),digits=5)) m³ a⁻¹ no bedrock limitation
                           
                            Till production (mean): $(round(mean(ṁt_catch_d),digits=5)) m³ a⁻¹ 
                                area-scaled (mean): $(round(mean(ṁt_rate_d), digits=5)) mm a⁻¹
                           
                         Sediment discharge (mean): $(round(mean(Qbs_catch_d),digits=5)) m³ a⁻¹
                                area-scaled (mean): $(round(mean(Qbs_rate_d) ,digits=5)) mm a⁻¹

                    ==============================================================

                            $(run_name[2]) glacier

                            Erosion rate (mean): $(round(mean(ϵ̇_rate_c),digits=5)) mm a⁻¹ no bedrock limitation
                            Erosion rate (mean): $(round(mean(ϵ̇_catch_c),digits=5)) m³ a⁻¹ no bedrock limitation
                           
                            Till production (mean): $(round(mean(ṁt_catch_c),digits=5)) m³ a⁻¹ 
                                area-scaled (mean): $(round(mean(ṁt_rate_c), digits=5)) mm a⁻¹
                           
                         Sediment discharge (mean): $(round(mean(Qbs_catch_c),digits=5)) m³ a⁻¹
                                area-scaled (mean): $(round(mean(Qbs_rate_c) ,digits=5)) mm a⁻¹

                    ==============================================================
                            
                            Two sample T-Test

                            Erosion rate: 

$(OneSampleZTest(ϵ̇_rate_c[:],ϵ̇_rate_d[:]))

                            Erosion quantity: 

$(OneSampleZTest(ϵ̇_catch_c[:],ϵ̇_catch_d[:]))

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
                            Till production:

 $(OneSampleZTest(ṁt_catch_c[:],ṁt_catch_d[:]))
                                
                                area-scaled: 

$(OneSampleZTest(ṁt_rate_c[:],ṁt_rate_d[:]))


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


                         Sediment discharge: 

$(OneSampleZTest(Qbs_catch_c[:], Qbs_catch_d[:]))
                                

                        area-scaled: 

$(OneSampleZTest(Qbs_rate_c[:] , Qbs_rate_d[:]))
                        

Note for below: Erosion (area-scaled and catch) of low erosion and high erosion has pvalue     
     outcome with 95% confidence: reject h_0
      two-sided p-value:           <1e-15
SEE HISTOGRAM Comments below



")
# figure()
# for i =1:length(run_name)
#     plot(sum(ṁt_mean[:,i],dims=1)*1000*maximum(diff(pg.sgrid))*yr, maximum(-Qbs_mean[:,i],dims=1)*yr, "*",label=run_name[i])

# end
# xlabel("ṁt (m³ a⁻¹")
# ylabel("Qₛ (m³ a⁻¹")
# legend()

# # shows that it depends on sediment transport, not necessarily erosion.... not characteristic of high erosion rates. 



dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )
        
       
    else
        push!(dep,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )

    end
end

trans= vcat(trans...)

dep =vcat(dep...)







println("

\\begin{table}
 \\caption{Model outputs}
  \\centering
  \\begin{tabular}{l c c c  |c c c }
    \\hline
                        & \\cl & \\db &  p & \\db & \\db & p \\\\
                        &     &       &    & bulb & no bulb & \\\\  \\hline
    n                     &$(length(ṁt_rate_c))  & $(length(ṁt_rate_d))  &  & $(length(dep[:,1]))& $(length(trans[:,1])) &  \\\\ 
                          &      &      &    &&& \\\\
 
  \$ \\dot{\\epsilon} \$ (\\mma) &$(round(mean(ϵ̇_rate_c),digits=2)) & $(round(mean(ϵ̇_rate_d),digits=2))  &  $(round(pvalue(MannWhitneyUTest(ϵ̇_rate_c[:],ϵ̇_rate_d[:])),digits=3))

&$(round(mean(dep[:,1]),digits=2))  & $(round(mean(trans[:,1]),digits=2)) &  \\textbf{$(round(pvalue(MannWhitneyUTest(trans[:,1],dep[:,1])),digits=3)) }

 \\\\
  \$ \\dot{m}_t \$  (\\mma)     &$(round(mean(ṁt_rate_c),digits=2))& $(round(mean(ṁt_rate_d),digits=2))  & \\textbf{ $(round(pvalue(MannWhitneyUTest(ṁt_rate_c[:],ṁt_rate_d[:])),digits=3)) }

&$(round(mean(dep[:,2]),digits=2))  & $(round(mean(trans[:,2]),digits=2)) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,2],dep[:,2])),digits=3)) }
   \\\\
  \$ \\dot{Q_s} \$ (\\mma)      &$(round(mean(Qbs_rate_c),digits=2))&$(round(mean(Qbs_rate_d),digits=2)) & \\textbf{ $(round(pvalue(MannWhitneyUTest(Qbs_rate_c[:],Qbs_rate_d[:])),digits=3))}

&$(round(mean(dep[:,3]),digits=2))  & $(round(mean(trans[:,3]),digits=2)) &  $(round(pvalue(MannWhitneyUTest(trans[:,3],dep[:,3])),digits=3))

  \\\\
                          &      &      &      &&\\\\
  \$ \\epsilon \$ (\\mmma)      &$(round(mean(ϵ̇_catch_c))) &$(round(mean(ϵ̇_catch_d))) & \\textbf{ $(round(pvalue(MannWhitneyUTest(ϵ̇_catch_c[:],ϵ̇_catch_d[:])),digits=3))}

&$(round(mean(dep[:,4])))  & $(round(mean(trans[:,4]))) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,4],dep[:,4])),digits=3)) }

    \\\\
  \$ m_t \$  (\\mmma)          &$(round(mean(ṁt_catch_c)))&$(round(mean(ṁt_catch_d))) &$(round(pvalue(MannWhitneyUTest(ṁt_catch_c[:],ṁt_catch_d[:])),digits=3))

&$(round(mean(dep[:,5])))  & $(round(mean(trans[:,5]))) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,5],dep[:,5])),digits=3))}

    \\\\
  \$ Q_s \$ (\\mmma)           &$(round(mean(Qbs_catch_c)))&$(round(mean(Qbs_catch_d)))& $(round(pvalue(MannWhitneyUTest(Qbs_catch_c[:],Qbs_catch_d[:])),digits=3))

&$(round(mean(dep[:,6])))  & $(round(mean(trans[:,6]))) &  $(round(pvalue(MannWhitneyUTest(trans[:,6],dep[:,6])),digits=3))

   \\\\

           &      &      &    &&& \\\\
 \$D_m \$ (\\cm) &      &      &    &$(round(mean(dep[:,7]),digits=4)*10) & $(round(mean(trans[:,7]),digits=4)*10) & \\textbf{ $(round(pvalue(MannWhitneyUTest(trans[:,7],dep[:,7])),digits=3)) }\\\\    \\hline
           &      &      &    &&& \\\\

 \\multicolumn{7}{l}{Bold font represents significance at the \$97.5\$\\% confidence interval with OneSampleZTest.}

  \\end{tabular}
\\end{table}


")
