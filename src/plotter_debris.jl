using Parameters
using PyPlot
import Roots: find_zero
using Dates
using JLD2
using StatsBase

using DelimitedFiles
include("post_proc_funs.jl")
#################"
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
end

yr= 365*86400


run_name=["debris_5deg_hw" "debris_2.5deg_hw" "debris"  "clean_5deg_hw" "clean_2.5deg_hw"  "clean"][3]

hts_debris, Qbs_debris, ϵ̇_debris, ṁt_debris, Dm_debris, zs_debris, zb_debris, H20_velocity_debris, ∂ϕ_∂x_debris=return_run(fp_load,run_name)



file_name="$(run_name)"
@load "$(fp_load)$(run_name)_12_outputs_50yrs.jld2" hts Qw Qbs zs_ zb_ ϵ̇ ṁt velocity  ∂ϕ_∂x spins  pg pp pn
H2O_velocity_debris=velocity;  ∂ϕ_∂x_debris= ∂ϕ_∂x; 

Qbs_out= maximum(abs.(Qbs_debris),dims=1)[:]*yr;
ϵ̇_catch = mean(ϵ̇_debris,dims=1)[:]*yr*1e3;
mt_year= (sum(ṁt_debris,dims=1)'*yr*1000*(pg.sgrid[2]-pg.sgrid[1]))[:]
ϵ̇_source=(sum(ϵ̇_debris,dims=1)'*yr*1000*(pg.sgrid[2]-pg.sgrid[1]))[:]

# load ice data

################
### plotting ###
################
figure(1,figsize=(7,3.5))
subplot(1,2,1)
plot([90000 ,  0], [90000, 0], color="black")
plot(ϵ̇_source,Qbs_out, "+")
xlim([0, 90000])
ylim([0, 90000])
xlabel("ϵ̇ source (m³ a⁻¹)")
ylabel("Qbs (m³ a⁻¹)")
tight_layout()

subplot(1,2,2)
semilogy(Dm_debris,ϵ̇_source./Qbs_out,  "+")
axhline(1,color="black")
#ylim([0.5, 5])
#xlim([0.13, .37])
ylabel("ϵ̇ source ÷ Qbs (-)")
xlabel("Dₘ (m)")
tight_layout()

##################

figure(3,figsize=(10,10)); clf()
subplot(4,1,1)
# p1=fill_between(ice_data_debris[1:160,1], reverse(ice_data_debris[1:160,8]),0, color="grey")
# ylabel("Debris H (m)", color="grey")
# xlim([pg.sgrid[1], pg.sgrid[end]])
# xticks([])


p1b = twinx()
#plot
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]),pg.ht0,color="black")
#semilogy(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), hts_debris, color="blue")
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), hts_debris, color="blue")
ylabel("till (m)")
xticks([])
title("$(file_name)")

p2=subplot(4,1,2)
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), ṁt_debris.*yr*1e3, color="blue",  label="debris")
ylabel("ṁt (mm a⁻¹)")
xticks([])
legend()

p2b = twinx()
plot(pg.sgrid, ϵ̇_debris.*yr*1e3, color="blue",   ls=":",label="debris")

ylabel("ϵ̇ (mm a⁻¹)")
xticks([])

subplot(4,1,3, sharex=p2)
yticks([])
p1b = twinx()
plot(pg.sgrid,abs.(Qbs_debris)*yr, color="blue")
ylabel("Qs (m³ a⁻¹)")
legend()
xticks([])



subplot(4,1,4, sharex=p2)
plot(ice_data_debris[1:160,1], reverse(ice_data_debris[1:160,4]),0, color="blue",  ls=":",label=L"u$_b$")
ylabel("Ub (m a⁻¹)", color="black")
xlim([pg.sgrid[1], pg.sgrid[end]])
xticks([])
legend()

p4=twinx()

p1=plot(pg.sgrid,abs.(H2O_velocity_debris), color="blue",label="water")
ylabel(" H₂O vel. (m s⁻¹)")
xticks(range(pg.sgrid[1],stop=pg.sgrid[end]+50,length=5))
legend()

tight_layout()
subplots_adjust(hspace=0.0)

savefig("$(fp_save)debris_outputs.pdf")


tmp_debris = maximum(findall(x->isnan(x),ϵ̇_debris))-1
glac_area_debris = 1000*(abs(pg.sgrid[1]-pg.sgrid[tmp_debris]))

println("

        =======================================================================

                Debris glacier

                Erosion rate: $(round(mean(ϵ̇_debris[5:end-5]) *yr*1e3,digits=4)) mm a⁻¹ no bedrock limitation
               
                Till production:  $(round(mean(ṁt_debris[5:end-5])*yr*glac_area_debris,digits=4)) m³ a⁻¹ 
                    area-scaled:  $(round(mean(ṁt_debris[5:end-5])*yr*1e3,digits=4)) mm a⁻¹
               
             Sediment discharge: $(round(abs(Qbs_debris[4])*yr,digits=4)) m³ a⁻¹
            
            ")
