# Bench-like synthetic run
if homedir()=="/users/idelaney"
    using Pkg; Pkg.activate(".")
end
using Parameters
using PyPlot
using Dierckx
using DifferentialEquations
import Roots: find_zero
using Dates
using JLD2
using DelimitedFiles
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,spinner, Num, Phys

if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/data/debris/"
    ice_data_loc="glaciers/"
elseif homedir()=="/users/idelaney"
    fp_save=     "/scratch/idelaney/data/debris/"
    ice_data_loc="glaciers/"
end

spin_threshold = 1e-10; break_time=Int(100)

include("interface-ice.jl")
### helper functions and SUGSET updates for this runner    
#############################
### run model and process ###
#############################

run_name=["debris"   "clean" "debris_cleanmelt"][end]
parameter_combo=["debris" "clean"][2]

pg = Mountain(
              tspan_spin =(0,year),
              tspan=(0,year)
              )
                 
include("file_load.jl")
pg = Mountain(pg,
              till_growth_lim =0.25,
              till_lim =0.5,
              lake_level=0.0,
              stoch= false,
              sgrid=range(pg.domain[1],stop=pg.domain[2],length=350),
              )
pp= Phys(pp)

pg = Mountain(pg,
              ds= abs(pg.sgrid[2]-pg.sgrid[1]),
              )

pn =Num(Δσ=1e-3)
error()

lower  = [0.01 1e-8 4]
upper  = [0.20 1e-4 0.5]

include("parameter_search.jl")
parameter_search_ice(pp, pg, pn, fp_save,spin_threshold,break_time,run_name,parameter_combo)

