# Bench-like synthetic run

##################
### parameters ###
##################b
DAY = 86400

lower = [0.01  1e-8  0.75    0.5*DAY  1e-5]

upper = [0.15  1e-3  3   4*DAY   0.01]

mids = [(upper[1]-lower[1])*0.5
        6e-7
        1.75
        (upper[4]-lower[4])*0.5
        0.001
        ]


include("parameter_search.jl")

results =[]

push!(results,parameter_search_issm(pp, pg, pn, fp_save))
