
# Bench-like synthetic run
using Parameters
using PyPlot
using Dierckx
using DifferentialEquations
import Roots: find_zero
using Dates
using JLD2
using DelimitedFiles
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,spinner, Num, Phys

if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    ice_data_loc="/Users/Ian/data/debris/"
elseif homedir()=="/users/idelaney"
    fp_save="/users/idelaney/scratch/idelaney/data/debris/"
    ice_data_loc="/users/idelaney/data/debris/"
end


include("interface-ice.jl")
### helper functions and SUGSET updates for this runner    
#############################
### run model and process ###
#############################
file_name="short_low_erosion_slowish"

run_name=["debris" "clean" "both"][3]
pn = Num()


if run_name=="clean" ||   run_name =="both"
    ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_free_example_for_ian.csv", ',',skipstart=1)
    ice_surf_spl, ice_base_spl, ice_ub_spl, ice_qw_spl,ice_sdeb_spl = ice_model_load(ice_data,1,155,2)
    pg = Mountain(tspan_spin =(0,year),
                   tspan=(0,year),
                  domain = (0.0,maximum(ice_data[1:155,1])),
                  )

    #ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine_headwall_2point5_deg_bed.csv", ',',skipstart=1)
    #ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine_headwall_5_deg_bed.csv", ',',skipstart=1)
    
    #    ice_surf_spl, ice_base_spl, ice_ub_spl, ice_qw_spl,ice_sdeb_spl = ice_model_load(ice_data,265,1310,5)

    pg = Mountain(pg,
                  till_growth_lim =0.25,
                  till_lim =0.5,
                  sgrid=range(pg.domain[1],stop=pg.domain[2],length=350),
                  ht0 = zeros(350-1)  
                  )

    hts = pg.ht0

    include("interface-ice.jl")

    sgrid = reverse(pg.sgrid)
    Qw_clean  = SUGSET.Q_w(sgrid, 0.0, pg, pp)# Qw_clean[1:116] .= NaN
    Qsc_clean = SUGSET.sed_transport(sgrid, 0.0,pg,pp, pn)[1] # Qsc_clean[1:116] .= NaN
    ṁw_clean  = abs.(SUGSET.source_water.(sgrid, Ref(0.0), Ref(pg), Ref(pp)))# ṁw_clean[1:116] .= NaN
    zs_clean  = SUGSET.zs.(sgrid, Ref(0.0) , Ref(pg))# 
    zb_clean  = SUGSET.zb.(sgrid, Ref(0.0) , Ref(pg))# 
    ϵ̇_clean   = SUGSET.source_till.(sgrid, Ref(0.0), Ref(pg), Ref(pp))# ϵ̇_clean[1:116] .= NaN
    ub_clean  = SUGSET.basal_velocity.(sgrid, Ref(0.0), Ref(pg), Ref(pp))# ub_clean[1:116] .= NaN
    ṁt_clean  =  SUGSET.effective_sed_source.(sgrid[1:end-1], Ref(0.0), Ref(pg), Ref(pp), Ref(pn), hts )# ṁt_clean[1:116] .= NaN
    velocity_clean  = Qw_clean  ./ SUGSET.xsect(sgrid, 0.0, pg, pp, pn)[1]# velocity_clean[1:116] .= NaN
    ∂ϕ_∂x_clean = SUGSET.Ψˣ.(sgrid,Ref(0.0),Ref(pg),Ref(pp))# ∂ϕ_∂x_clean[1:116] .= NaN
end

if run_name =="debris" ||   run_name =="both"
    ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine.csv", ',',skipstart=1)
    ice_surf_spl, ice_base_spl, ice_ub_spl, ice_qw_spl,ice_sdeb_spl = ice_model_load(ice_data,5,1550,4)
    pg = Mountain(tspan_spin =(0,year),
                   tspan=(0,year),
                   domain = (0.0,maximum(ice_data[1:1550,1])),
                   )

    #ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine_headwall_2point5_deg_bed_2x_headwall_erosion.csv", ',',skipstart=1)
    # ice_data = DelimitedFiles.readdlm("$(ice_data_loc)debris_cover_example_for_ian_fine_headwall_5_deg_bed_2x_headwall_erosion.csv", ',',skipstart=1)


    pg = Mountain(pg,
                  till_growth_lim =0.25,
                  till_lim =0.5,
                  sgrid=range(pg.domain[1],stop=pg.domain[2],length=350),
                  ht0 = zeros(350-1)  
                  )

    hts = pg.ht0

    include("interface-ice.jl")

    sgrid = reverse(pg.sgrid)
    
    Qw_debris  = SUGSET.Q_w(sgrid, 0.0, pg, pp)# Qw_debris[1:13] .= NaN
    Qsc_debris = SUGSET.sed_transport(sgrid, 0.0,pg,pp, pn)[1] # Qsc_debris[1:13] .= NaN
    ṁw_debris  = abs.(SUGSET.source_water.(sgrid, Ref(0.0), Ref(pg), Ref(pp)))# ṁw_debris[1:13] .= NaN
    zs_debris  = SUGSET.zs.(sgrid, Ref(0.0) , Ref(pg))# 
    zb_debris  = SUGSET.zb.(sgrid, Ref(0.0) , Ref(pg))# 
    ϵ̇_debris   = SUGSET.source_till.(sgrid, Ref(0.0), Ref(pg), Ref(pp))# ϵ̇_debris[1:13] .= NaN
    ub_debris  = SUGSET.basal_velocity.(sgrid, Ref(0.0), Ref(pg), Ref(pp))# ϵ̇_debris[1:13] .= NaN
    ṁt_debris  =  SUGSET.effective_sed_source.(sgrid[1:end-1], Ref(0.0), Ref(pg), Ref(pp), Ref(pn), hts )# ṁt_debris[1:13] .= NaN
    velocity_debris  = Qw_debris  ./ SUGSET.xsect(sgrid, 0.0, pg, pp, pn)[1]# velocity_debris[1:13] .= NaN
    ∂ϕ_∂x_debris = SUGSET.Ψˣ.(sgrid,Ref(0.0),Ref(pg),Ref(pp))# ∂ϕ_∂x_debris[1:13] .= NaN
    surf_debris = surf_deb_t.(sgrid, Ref(0.0), Ref(pg))#    surf_debris[1:13] .= NaN

    
end
lake_x =sgrid[1]
for i=1:length(sgrid)
    if zs_debris[i] <= pg.lake_level
        global lake_x = sgrid[i]
    end end

sgrid=reverse(sgrid)

#clf()
figure(figsize=(8.1,4.5))
ax=subplot(2,2,1)
plot(   sgrid,zs_debris, color="orange",label= "debris")
p1=plot(sgrid, zs_clean, color="blue",  label= "clean")
plot(lake_x,pg.lake_level, "*", markersize=10)
fill_between(sgrid,zb_clean, minimum(zb_clean), color="black")
text(sgrid[17], 1.01*maximum(zs_clean), "a")
ylim([minimum(zs_debris),maximum(zs_debris)*1.05])
xlim([sgrid[1],16000])
xticks([])
ylabel("Elevation (m)")
legend(loc=1)

subplot(2,2,2)
ax1=fill_between(sgrid,surf_debris, 0, color="sienna")
ylabel("Debris (m)", color="sienna")
text(sgrid[17], 2.5, "c")
ylim([0.001, 2.7] )
xlim([sgrid[1],16000])
xticks([])

pt=twinx()

axa=plot(sgrid, ub_debris*year,color="orange")
plot(sgrid, ub_clean*year,color="blue")
ylim([0.1, 7.6 ])
xlim([sgrid[1],16000])
xticks([])
ylabel(L"$u_b$ (m a⁻¹)")

subplot(2,2,3)
legend(loc=6)

axvspan(9800,12340 ,color="grey")
plot(sgrid[1:end-17], ∂ϕ_∂x_debris[1:end-17],color="orange")
plot(sgrid[1:end-130], ∂ϕ_∂x_clean[1:end-130],color="blue")
xlim([sgrid[1],16000])
ylabel(L" $\frac{\partial \phi}{\partial x}$ (Pa  m⁻¹)")
text(sgrid[17], 1400, "b")
axb = twinx()
ax4=plot(sgrid[1:end-23],    diff(∂ϕ_∂x_debris[1:end-22])  ./pg.ds,color="orange",":")
plot(    sgrid[1:end-145], diff(∂ϕ_∂x_clean[1:end-144])./pg.ds, color="blue"  ,":")
xlim([sgrid[1],16000])
axhspan(0, -2, 0.0, 17000,color="lightgrey")
ylim([-.063, .79])
ylabel(L" $\frac{\partial^2 \phi}{\partial^2 x}$ (Pa m⁻²)")
text(12340*1.01,0.1,"Bulb location  \n (see Fig. 2)",rotation=270)
xticks([0,  4000, 8000 , 12000 , 16000])

subplot(2,2,4)

plot([0, 1], [0,1], color ="black",":", label=L" $\mathrm{Q_w}$")
plot([0, 1], [0,1], color ="black", label=L"$\dot{m}_w$")
text(sgrid[17], 3.5, "d")

plot(sgrid[1:end-13+1], reverse(abs.(Qw_debris[13:end])),color="orange", ":")
plot(sgrid[1:236], reverse(abs.(Qw_clean[115:end])) ,color="blue", ":")
ylim([.01, 3.9])
xlim([sgrid[1],16000])
legend(loc=6)
ylabel(L" $\mathrm{Q_w}$ (m³  s⁻¹)")
axvspan(9800,12340 ,color="grey")
text(12340*1.01,0.5,"Bulb location  \n (see Fig. 2)",rotation=270)

axb = twinx()
ax1=plot(sgrid[1:end-14], ṁw_debris[1:end-14]*year,color="orange")
plot(    sgrid[1:end-116], ṁw_clean[1:end-116]*year ,color="blue")
ylim([0.5, 29.9])
xlim([sgrid[1],16000])
#xticks([])
ylabel(L"$\dot{m}_w$ (m a⁻¹)")
xticks([0,  4000, 8000 , 12000 , 16000])


tight_layout()
subplots_adjust(hspace=0.0, wspace=.42)


savefig("$(fp_save)model_setup.pdf")

@save "$(fp_load)model_setup.jld2" sgrid zb_clean zb_debris zs_clean zs_debris ṁw_debris ṁw_clean Qw_clean Qw_debris ∂ϕ_∂x_debris ∂ϕ_∂x_clean ub_debris ub_clean surf_debris
error()



dx=abs(ice_data[1,1]-ice_data[2,1])

∂ϕ_import = SUGSET.ri * SUGSET.g *(diff(ice_data[:,2])./dx .- diff(ice_data[:,3])./dx ) .+ SUGSET.rw*SUGSET.g*diff(ice_data[:,3])./dx  # .- 800
plot(ice_data[1:154,1], abs.(reverse(∂ϕ_import[1:154])), label = "import" )
plot(sgrid,∂ϕ_∂x, label = "SUGSET")
xlim([sgrid[1], sgrid[end]])
#xlim([sgrid[1],2000])
ylim([sgrid[1], 4500])
legend()
