    using Parameters, Dierckx, Interpolations, NCDatasets
using Roots
using Random
using Dates
using Statistics
using VAWTools
using Interpolations
using Test
using Distributions

@with_kw struct Mountain<:Glacier @deftype Float64
    # The spatial domain:
    domain::Tuple{Float64, Float64}=(0.0, 6000.0)
    # Source terms
    mw = 0 # water source term
    Mw = 0 # water source if stead
    mb = 0 #.03/(86400*365) # sediment source term meters per year...
    Mb0 = 0
    # the finite volume grid (the boundaries not cell-centers):
    sgrid::LinRange{Float64}=range(domain[1],stop=domain[2],length=350)
    ds = abs(sgrid[2] - sgrid[1])

    # Time domain
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}=(0.0, 1.0)
    tout::Vector{Float64}=tspan[1]:0.25*day:tspan[2] # output times
    #@assert tout[1]>=tspan[1]
    #@assert tout[2]<=tspan[2]

    # IC & BC
    ht0::Vector{Float64}=sgrid[1:end-1]*0.0 .+ 5e-2 # initial till layer thickness
    zb0::Vector{Float64}=sgrid[1:end-1]*0.0 .+ .1 # initial till layer thickness
    Q_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top

    # source averaging
    source_average_time=day*2.5
    source_quantile = 0.75

    #till
    till_growth_lim = 0.75 # maximium amount of till, above which till stops being produced.
    fsl = 1.0# fraction of basal sliding
    till_lim = 1.0
    stoch = false # implement stochastic transport

    # misc p
    # Topo
    para = 0.05
    spin::Bool = false
    spin_fac = 1
    # misc parameters
    float_fracˣ = 0.99 # 1<=>overburden pressure, 0<=>atmospheric
    lake_level=0
    @assert 0<=float_fracˣ<1

    gamma= -0.0008./year # mass balance gradient per second
    B_0_term = 0
end
pp= Phys(
    fi=5,
    ft=5,
    hookeangle=π/4)


function ice_model_load(ice_data,start,end_pt,gap)
    ice_length = ice_data[start:gap:end_pt,1]
    ice_surf   = reverse(ice_data[start:gap:end_pt,2])
    ice_thick  = reverse(ice_data[start:gap:end_pt,3])
    ice_ub     = reverse(ice_data[start:gap:end_pt,4]./year)
    ice_base   = reverse(ice_data[start:gap:end_pt,5])
    ice_ṁw     = reverse(-ice_data[start:gap:end_pt,6]./(.25*year))
    ice_surf_deb = reverse(ice_data[start:gap:end_pt,8])

    ###################################
    ### define sources and geometry ###
    ###################################
    
    ice_surf_spl  = Dierckx.Spline1D(ice_length, ice_surf,    k=5, bc="nearest", s=80) # import surfacew
    ice_base_spl  = Dierckx.Spline1D(ice_length, ice_base,    k=5, bc="nearest") # import base topo
    ice_ub_spl    = Dierckx.Spline1D(ice_length, abs.(ice_ub),k=3, bc="nearest", s=0) #import ub
    ice_qw_spl    = Dierckx.Spline1D(ice_length, ice_ṁw,      k=2, bc="nearest") # import surface
    ice_sdeb_spl  = Dierckx.Spline1D(ice_length, ice_surf_deb,k=2, bc="nearest") # import surface

    return ice_surf_spl, ice_base_spl,  ice_ub_spl, ice_qw_spl,ice_sdeb_spl
end

function SUGSET.basal_velocity(s, t, pg::Mountain , pp::Phys) 
    if thick(s,t,pg) < SUGSET.proglacial
        out = 0.0
    else
        out= abs(Dierckx.evaluate(ice_ub_spl, s))
    end
    return out
end

#TODO... Import topo information in to parameter file and spline from there... changing topography between runs
SUGSET.source_till(s, t, pg::Mountain , pp::Phys)    = SUGSET.erosion_herman(s,t,pg,pp)
SUGSET.source_water(s,t, pg::Mountain , pp::Phys)    = Dierckx.evaluate(ice_qw_spl, s)
SUGSET.source_water_sm(s,t, pg::Mountain , pp::Phys) = Dierckx.evaluate(ice_qw_spl, s)

SUGSET.zs(s, t, pg::Mountain )    = Dierckx.evaluate(ice_surf_spl, s) # get surface from interpolation
SUGSET.zb(s, t, pg::Mountain )    = Dierckx.evaluate(ice_base_spl, s)# get bed from interpolations
SUGSET.thick(s, t, pg::Mountain ) = max(5,zs(s,t,pg) - zb(s, t, pg))
function SUGSET.width(s, t, pg::Mountain )
    out = 1000.0
    return out 
end
surf_deb_t(s, t, pg::Mountain )   = Dierckx.evaluate(ice_sdeb_spl, s)
%

SUGSET.gradzs(s,t,pg::Mountain ) = derivative(ice_surf_spl,s)
SUGSET.gradzb(s,t,pg::Mountain ) = derivative(ice_base_spl,s)

function one_eval(pg,pp,pn,spin_threshold,break_time)
    println("starting model runs")

    hts,spins  = spinner(Mountain,pg,pp,pn,spin_threshold,break_time)

    pg = Mountain(pg,
                  stoch= false)

    
    Qbs  = SUGSET.dht_dt_fn(hts, pg.sgrid, 0.0, pg, pp, pn)[2]

    return hts,  Qbs, spins 
end

"""
            dphidx(s,t,pg,pp::Phys)

        Gives hydraulic gradient approximation using of Shreve potential (with
        a modification using `pg.float_frac`).
        """
function SUGSET.Ψˣ(s,t,pg::Mountain,pp::Phys)
    ff=pg.float_fracˣ #    *   VAWTools.sigmoid.((zs(s,t,pg) -  pg.lake_level) ,0,70)
    out =ff*ri*g*(SUGSET.gradzs(s,t,pg) - SUGSET.gradzb(s,t,pg))  + rw*g*SUGSET.gradzb(s,t,pg)

    ff=   VAWTools.sigmoid.((zs(s,t,pg) -  pg.lake_level) ,0,150)
    out = out*ff 
    
    return out 
end

