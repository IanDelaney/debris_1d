using Parameters
using PyPlot
import Roots: find_zero
using Dates
using JLD2
using StatsBase
using DelimitedFiles
using HypothesisTests
#using SUGSET
include("post_proc_funs.jl")
#################"
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
    ice_data_loc="/Users/Ian/data/debris/"
end
%
year=yr =365*86400
test= @isdefined Qbs_catch_d
run_name=["debris" "debris_cleanmelt"]

# # # load ice data
if test == false     
    include("ensemble_load.jl")
    
    @load "$(fp_load)model_setup.jld2" sgrid zb_debris  zs_debris ṁw_debris  Qw_debris  ∂ϕ_∂x_debris ub_debris surf_debris
    @load "/Users/Ian/data/debris/debris_cleanmelt_6_outputs_fixed_para_paracombo_debris_100yrs.jld2" Qw
    Qw_clean = -Qw

    smid = range(sgrid.start,sgrid.stop,length=sgrid.lendiv)
    sgrid = range(sgrid.start,sgrid.stop,length=sgrid.len)
end




figure(2,figsize=(9.6,6)); clf()
#subplot(3,1,1)
subplot2grid((8,3), (0,0), rowspan=2, colspan=3)


fill_between(   sgrid,0.0 .* zs_debris,zs_debris .- zb_debris, color="dimgrey",alpha=.75)

#ylim([minimum(zs_debris),maximum(zs_debris)*1.05])
xlim([sgrid[1],16000])
ylim([2.5, maximum(zs_debris .- zb_debris)*1.25])

xticks([])

ylabel("Ice thickness \n (m)")
xticks([])

p1b = twinx()

plot(sgrid,ub_debris*yr, color="dimgrey", ":" ,linewidth=3)
plot(sgrid[1:end-10],reverse(Qw_clean)[1:end-10], color="darkgreen",  linewidth=3)
plot(sgrid[1:end-10],reverse(Qw_debris)[1:end-10], color="darkorange",linewidth=3)
ylim([0.2,9.5])
xlim([sgrid[1],16000])

xticks([])

ylabel("Water discharge \n (m³ s⁻¹) \n Basal velocity \n (m a⁻¹)",rotation=270, labelpad=45)
xticks([])

fill_between(-sgrid[1:2],[10^-15, 10^-14], [10^-15,10^-14], color ="black", alpha=.75, label="Ice Thickness")
plot([10^-15, 10^-14], [10^-15,10^-14], color ="black",lw=3,":", label="Basal Velocity")
plot([10^-15, 10^-14], [10^-15,10^-14], color ="black",lw=3, label="Water Discharge")
legend(loc=9)

text(400,7.5,"a")

subplot2grid((8,3), (2,0), rowspan=2, colspan=3)
axhline(mean(pg.ht0),color="black",linewidth=1,ls=":")
text(13000,mean(pg.ht0)+.01, "initial condition")

for i =1:length(run_name)
    if i==1
        plot(smid[1:end-10], reverse(hts_moy[:,i])[1:end-10],label=L"DEBRIS",color="darkorange",linewidth=3)
        fill_between(smid[1:end-10], reverse(hts_min[:,i])[1:end-10], reverse(hts_max[:,i])[1:end-10],color="darkorange",alpha=.5)
    else
        plot(smid[1:end-10], reverse(hts_moy[:,i])[1:end-10],label=L"DEBRIS\_CLEANMELT",color="darkgreen",linewidth=3)
        fill_between(smid[1:end-10], reverse(hts_min[:,i])[1:end-10], reverse(hts_max[:,i])[1:end-10],color="darkgreen",alpha=.5)
  
    end
end

text(400,.2,"b")


legend()
ylim([0.001, .27])
xlim([sgrid[1],16000])
ylabel("Till Height \n (m)")
xticks([])


subplot2grid((8,3), (4,0), rowspan=2, colspan=3)
axvspan(9800,12340 ,color="silver", alpha=.5)#

plot([10^-15, 10^-14], [10^-15,10^-14], color ="black",     lw=3, label="Erosion rate with till effects")
plot([10^-15, 10^-14], [10^-15,10^-14], color ="black","--", lw=3, label="Erosion rate without till effects")
legend(loc=1)

for i =1:length(run_name)
    if i==1
        semilogy(smid[1:end-9], reverse(ϵ̇_moy[:,i])[1:end-10]*yr*1e3,color="darkorange",ls="--",linewidth=3)
        semilogy(smid[1:end-10], reverse(ṁt_moy[:,i])[1:end-10]*yr*1e3,color="darkorange",linewidth=3)
        fill_between(smid[1:end-10], reverse(ṁt_min[:,i])[1:end-10]*yr*1e3, reverse(ṁt_max[:,i])[1:end-10]*yr*1e3,color="darkorange",alpha=.5)
 
    else
        
        semilogy(smid[1:end-9], reverse(ϵ̇_moy[:,i])[1:end-10]*yr*1e3,color="darkgreen",ls="--",linewidth=3)
        semilogy(smid[1:end-10], reverse(ṁt_moy[:,i])[1:end-10]*yr*1e3,color="darkgreen",linewidth=3)
        fill_between(smid[1:end-10], reverse(ṁt_min[:,i])[1:end-10]*yr*1e3, reverse(ṁt_max[:,i])[1:end-10]*yr*1e3,color="darkgreen",alpha=.5)

    end
end

text(400,10,"c")
text( 800, 9, "Glacier flow direction")
arrow(1000, 7,2000,0,width=.75,head_length=500,color="black")
# text(12000, 1.1, "Glacier terminus")
# arrow(12000,1,smid[end-109]-11700,-0.35,head_length=200, head_width=.125, color="black") 
# arrow(14300,1,smid[end-9]-14600,-0.3,head_length=200, head_width=.125, color="black") 


xlim([sgrid[1],16000])
ylim([.5, 3*maximum(ṁt_max)*yr*1e3])
ylabel("Erosion Rate \n (mm a⁻¹)")
xticks([])


subplot2grid((8,3), (6,0), rowspan=2, colspan=3)
axvspan(9800,12340 ,color="silver", alpha=.5)
for i =length(run_name):-1:1
    if i==1
        plot(sgrid[1:end-9], reverse(-Qbs_moy[10:end,i]*yr),color="darkorange",linewidth=3)
        fill_between(sgrid[1:end-9],reverse( -Qbs_min[10:end,i]*yr),reverse( -Qbs_max[10:end,i]*yr),color="darkorange",alpha=.5)
    else
        plot(sgrid[1:end-9], reverse(-Qbs_moy[10:end,i]*yr),color="darkgreen",linewidth=3)
        fill_between(sgrid[1:end-9],reverse( -Qbs_min[10:end,i]*yr),reverse( -Qbs_max[10:end,i]*yr),color="darkgreen",alpha=.5)
    end
end
text(400,41000,"d")
ylim([10, 50000])
xlim([sgrid[1],16000])
xticks([0,  4000, 8000 , 12000 , 16000])
ylabel("Sediment \n Discharge \n (m³ a⁻¹)")
xlabel("x (m)")


text(reverse(sgrid)[96]*1.01,1,"  Bulb location \n     (see text)",rotation=270)

tight_layout()
#subplots_adjust(bottom=0.15)
subplots_adjust(hspace=0)

savefig("$(fp_save)model_outputs_debris_cleanmelt.pdf")



###############
dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,ϵ̇_rate_d[i])
       
    else
        push!(dep,ϵ̇_rate_d[i])
    end
end


dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,Dm_d[i])
       
    else
        push!(dep,Dm_d[i])
    end
end


println("
                    ==============================================================

                            $(run_name[1]) glacier

                            Erosion rate (mean): $(round(mean(ϵ̇_rate_d),digits=5)) mm a⁻¹ no bedrock limitation
                            Erosion rate (mean): $(round(mean(ϵ̇_catch_d),digits=5)) m³ a⁻¹ no bedrock limitation
                           
                            Till production (mean): $(round(mean(ṁt_catch_d),digits=5)) m³ a⁻¹ 
                                area-scaled (mean): $(round(mean(ṁt_rate_d), digits=5)) mm a⁻¹
                           
                         Sediment discharge (mean): $(round(mean(Qbs_catch_d),digits=5)) m³ a⁻¹
                                area-scaled (mean): $(round(mean(Qbs_rate_d) ,digits=5)) mm a⁻¹

                    ==============================================================

                            $(run_name[2]) glacier

                            Erosion rate (mean): $(round(mean(ϵ̇_rate_c),digits=5)) mm a⁻¹ no bedrock limitation
                            Erosion rate (mean): $(round(mean(ϵ̇_catch_c),digits=5)) m³ a⁻¹ no bedrock limitation
                           
                            Till production (mean): $(round(mean(ṁt_catch_c),digits=5)) m³ a⁻¹ 
                                area-scaled (mean): $(round(mean(ṁt_rate_c), digits=5)) mm a⁻¹
                           
                         Sediment discharge (mean): $(round(mean(Qbs_catch_c),digits=5)) m³ a⁻¹
                                area-scaled (mean): $(round(mean(Qbs_rate_c) ,digits=5)) mm a⁻¹

                    ==============================================================
                            
                            Two sample T-Test

                            Erosion rate: 

$(OneSampleZTest(ϵ̇_rate_c[:],ϵ̇_rate_d[:]))

                            Erosion quantity: 

$(OneSampleZTest(ϵ̇_catch_c[:],ϵ̇_catch_d[:]))

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
                            Till production:

 $(OneSampleZTest(ṁt_catch_c[:],ṁt_catch_d[:]))
                                
                                area-scaled: 

$(OneSampleZTest(ṁt_rate_c[:],ṁt_rate_d[:]))


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


                         Sediment discharge: 

$(OneSampleZTest(Qbs_catch_c[:], Qbs_catch_d[:]))
                                

                        area-scaled: 

$(OneSampleZTest(Qbs_rate_c[:] , Qbs_rate_d[:]))
                        

Note for below: Erosion (area-scaled and catch) of low erosion and high erosion has pvalue     
     outcome with 95% confidence: reject h_0
      two-sided p-value:           <1e-15
SEE HISTOGRAM Comments below



")
# figure()
# for i =1:length(run_name)
#     plot(sum(ṁt_mean[:,i],dims=1)*1000*maximum(diff(pg.sgrid))*yr, maximum(-Qbs_mean[:,i],dims=1)*yr, "*",label=run_name[i])

# end
# xlabel("ṁt (m³ a⁻¹")
# ylabel("Qₛ (m³ a⁻¹")
# legend()

# # shows that it depends on sediment transport, not necessarily erosion.... not characteristic of high erosion rates. 



dep=[];trans=[]
for i=1:length(ṁt_catch_d)
    if hts_bulb_d[i] <5e-2
        push!(trans,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )
        
       
    else
        push!(dep,[ϵ̇_rate_d[i] ṁt_rate_d[i] Qbs_rate_d[i]  ϵ̇_catch_d[i] ṁt_catch_d[i] Qbs_catch_d[i] Dm_d[i]] )

    end
end

trans= vcat(trans...)

dep =vcat(dep...)

