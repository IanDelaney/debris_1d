using Parameters
using PyPlot
import Roots: find_zero
using Dates
using JLD2
using StatsBase
using DelimitedFiles
using HypothesisTests
include("post_proc_funs.jl")
#################"
### load data ###
#################
if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/research/debris_erosion/figs/"
    fp_load="/Users/Ian/data/debris/"
end

yr= 365*86400
test= @isdefined Qbs_catch_d
   run_name=["debris" "clean"]
if test == false    
    include("ensemble_load.jl")
    @load "$(fp_load)model_setup.jld2" sgrid zb_clean zb_debris zs_clean zs_debris ṁw_debris ṁw_clean Qw_clean Qw_debris ∂ϕ_∂x_debris ∂ϕ_∂x_clean ub_debris ub_clean surf_debris
end
# # # load ice data

##################
smid  = pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1])
sgrid = pg.sgrid


figure(1,figsize=(9.8,7.5)); clf()
subplot(2,2,1)
plot(reverse(abs.(Qbes_moy[14:4:end-3,1])).*yr,  ∂ϕ_∂x_debris[4:4:end-10-3],color="black","*")
xlabel("Qsc")
ylabel("Hydro. pot.")
println("$(cor(reverse(abs.(Qbes_moy[14:4:end-3,1])).*yr,  ∂ϕ_∂x_debris[4:4:end-10-3]))")


subplot(2,2,2)
plot(abs.(Qbes_moy[14:end-3,1]).*yr,  Qw_debris[14:end-3],color="black","*")
xlabel("Qsc")
ylabel("Qws")
println("$(cor(abs.(Qbes_moy[14:end-3,1]).*yr,  Qw_debris[14:end-3]))")

subplot(2,2,3)
plot(reverse(abs.(Qbes_moy[12+3:end-3,1])).*yr,  diff(∂ϕ_∂x_debris[4:end-10-3])./pg.ds,color="black","*")
xlabel("Qsc")
ylabel("Curvature \n Hydro. pot.")
println("$(cor(abs.(Qbes_moy[12+3:end-3,1]).*yr,  diff(∂ϕ_∂x_debris[4:end-10-3]./pg.ds)))")


subplot(2,2,4)
plot(reverse(abs.(Qbes_moy[14:end-3,1])).*yr,   ṁw_debris[14:end-3]*yr,color="black","*")
xlabel("Qsc")
ylabel("ṁw")
println("$(cor(abs.(Qbes_moy[14:end-3,1]).*yr,   ṁw_debris[14:end-3]*yr))")

##################
### Qs Total
##################
println(" 


Qs total")
figure(3,figsize=(9.8,7.5)); clf()
subplot(2,2,1)
plot(reverse(abs.(Qbs_moy[14:4:end-3,1])).*yr,  ∂ϕ_∂x_debris[4:4:end-10-3],color="black","*")
xlabel("Qsc")
ylabel("Hydro. pot.")
println("$(cor(reverse(abs.(Qbs_moy[14:4:end-3,1])).*yr,  ∂ϕ_∂x_debris[4:4:end-10-3]))")


subplot(2,2,2)
plot(abs.(Qbs_moy[14:end-3,1]).*yr,  Qw_debris[14:end-3],color="black","*")
xlabel("Qsc")
ylabel("Qws")
println("$(cor(abs.(Qbs_moy[14:end-3,1]).*yr,  Qw_debris[14:end-3]))")

subplot(2,2,3)
plot(reverse(abs.(Qbs_moy[12+3:end-3,1])).*yr,  diff(∂ϕ_∂x_debris[4:end-10-3])./pg.ds,color="black","*")
xlabel("Qsc")
ylabel("Curvature \n Hydro. pot.")
println("$(cor(abs.(Qbs_moy[12+3:end-3,1]).*yr,  diff(∂ϕ_∂x_debris[4:end-10-3]./pg.ds)))")


subplot(2,2,4)
plot(reverse(abs.(Qbs_moy[14:end-3,1])).*yr,   ṁw_debris[14:end-3]*yr,color="black","*")
xlabel("Qsc")
ylabel("ṁw")
println("$(cor(abs.(Qbs_moy[14:end-3,1]).*yr,   ṁw_debris[14:end-3]*yr))")



###############
### Qsc Bulb
###############

println("


Just the bulb")
figure(4,figsize=(9.8,7.5)); 

subplot(2,2,1)
plot(reverse(abs.(Qbes_moy[215:290,1])).*yr,  ∂ϕ_∂x_debris[215:290],color="black","*")
xlabel("Qsc")
ylabel("Hydro. pot.")
println("$(cor(reverse(abs.(Qbes_moy[215:290,1])).*yr,  ∂ϕ_∂x_debris[215:290]))")


subplot(2,2,2)
plot(abs.(Qbes_moy[215:290,1]).*yr,  Qw_debris[215:290],color="black","*")
xlabel("Qsc")
ylabel("Qws")
println("$(cor(abs.(Qbes_moy[215:290,1]).*yr,  Qw_debris[215:290]))")

subplot(2,2,3)
plot(reverse(abs.(Qbes_moy[216:290,1])).*yr,  diff(∂ϕ_∂x_debris[215:290])./pg.ds,color="black","*")
xlabel("Qsc")
ylabel("Curvature \n Hydro. pot.")
println("$(cor(abs.(Qbes_moy[216:290,1]).*yr,  diff(∂ϕ_∂x_debris[215:290]./pg.ds)))")


subplot(2,2,4)
plot(reverse(abs.(Qbes_moy[215:290,1])).*yr,   ṁw_debris[215:290]*yr,color="black","*")
xlabel("Qsc")
ylabel("ṁw")
println("$(cor(abs.(Qbes_moy[215:290,1]).*yr,   ṁw_debris[215:290]*yr))")

###############
### QS Bulb
###############

println("


Qs Just the bulb")
figure(5,figsize=(9.8,7.5)); 

subplot(2,2,1)
plot(reverse(abs.(Qbs_moy[215:290,1])).*yr,  ∂ϕ_∂x_debris[215:290],color="black","*")
xlabel("Qs")
ylabel("Hydro. pot.")
println("$(cor(reverse(abs.(Qbs_moy[215:290,1])).*yr,  ∂ϕ_∂x_debris[215:290]))")


subplot(2,2,2)
plot(abs.(Qbs_moy[215:290,1]).*yr,  Qw_debris[215:290],color="black","*")
xlabel("Qsc")
ylabel("Qws")
println("$(cor(abs.(Qbs_moy[215:290,1]).*yr,  Qw_debris[215:290]))")

subplot(2,2,3)
plot(reverse(abs.(Qbs_moy[216:290,1])).*yr,  diff(∂ϕ_∂x_debris[215:290])./pg.ds,color="black","*")
xlabel("Qs")
ylabel("Curvature \n Hydro. pot.")
println("$(cor(abs.(Qbs_moy[216:290,1]).*yr,  diff(∂ϕ_∂x_debris[215:290]./pg.ds)))")


subplot(2,2,4)
plot(reverse(abs.(Qbs_moy[215:290,1])).*yr,   ṁw_debris[215:290]*yr,color="black","*")
xlabel("Qs")
ylabel("ṁw")
println("$(cor(abs.(Qbs_moy[215:290,1]).*yr,   ṁw_debris[215:290]*yr))")
