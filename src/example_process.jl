using PyPlot
using NCDatasets
using StatsBase
using Statistics
using JLD
using SUGSET
using HypothesisTests
using MultivariateStats
using Dates
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year

# using HypothesisTests
# using MultivariateStats

#const day = 86400
#Const = 365
#################
### Load data ###
#################
run_sugset = true


if run_sugset ==true

    include("interface-issm.jl")

    pn=Num()

    ht0_thing= zeros(length(pg.sgrid)-1)

    for i = 1:length(pg.sgrid)-1
        ht0_thing[i] = min(max(SUGSET.source_till(pg.sgrid[i],pg.tout[2],pg,pp)*SUGSET.year*200, 1e-3),pg.till_lim)
        if SUGSET.thick(pg.sgrid[i],pg.tout[1],pg) < 10
            ht0_thing[i]=0
        end
    end

    pg = Valley(pg,
                till_growth_lim =0.25,
                till_lim =0.5,
                tspan=(issm_time[start_year], issm_time[start_year+6]),
                ht0=ht0_thing
                )

    pg = Valley(pg,
                tout=pg.tspan[1]:day:pg.tspan[2] # make the last year of the ru
                )

    Qws_lo = zeros(length(pg.sgrid), length(pg.tout))

    println("starting run_model()")
    println(Dates.now())
    @time sol  = run_model(pg, pp, pn)
    println("run_model() finished")

    hts, dht_dts, sed_source, Qws, Qws_sm, Qbs, Qbes  = SUGSET.post_proc(sol,pg, pp, pn)
end


fs=14
check_pt = length(pg.tout)-length(pg.tout)÷12

zs_=zs.(pg.sgrid,Ref(pg.tout[1]),Ref(pg))
zb_=zb.(pg.sgrid,Ref(pg.tout[1]),Ref(pg))
width_=width.(pg.sgrid[1:end-1] .+ 0.5 .* (pg.sgrid[1]-pg.sgrid[2]),Ref(pg.tout[1]),Ref(pg))

source_out_H20=zeros(length(pg.sgrid))

for i = 1:length(source_out_H20)
    source_out_H20[i]=maximum(-Qws[i,:])
end


sgrid = pg.sgrid/1000

figure("profile at time $(pg.tout[check_pt]/year)", figsize=(5.5*1.6,5))
ax = subplot(3,1,1)
plot(sgrid, zs_ .+ (zs_ .- zb_)*3, linewidth=1,color="blue")
fill_between(sgrid,zs_ .+ (zs_.-zb_)*3   ,    0,color="skyblue")
plot(sgrid, zb_, linewidth=1, color="black")
fill_between(sgrid,zb_,color="black")
xlim([0,maximum(sgrid)])
ylim([100, maximum(zs_)+500])
setp(ax.get_xticklabels(),visible=false)
ylabel(L"$z$ ($\mathrm{m}$)",fontsize=fs)
#anchored_text = annotate("i", fontsize= fs*1.5, xy=(0.025, 0.8), xycoords="axes fraction")
tick_params(axis="both",labelsize=fs*0.8)


axb = ax.twinx()
axb.spines["right"]
plot(sgrid[1:end-1] .+ 0.5 .* (sgrid[1]-sgrid[2]), width_, linewidth=2,color="darkgrey")
ylabel(L"$w$ ($\mathrm{m}$)",color="darkgrey",fontsize=fs)
ylim([280,maximum(width_)+100])
tick_params(axis="both",labelsize=fs*0.8 )

ax1 = subplot(3,1,2, sharex=ax)
plot(sgrid[1:end-1] .+ 0.5 .* (sgrid[1]-sgrid[2]), hts[:,check_pt], linewidth=1)
fill_between(sgrid[1:end-1] .+ 0.5 .* (sgrid[1]-sgrid[2]),hts[:,check_pt], 0, color="red")
xlim([0,maximum(sgrid)])
ylim([0.0001, maximum(hts[:,check_pt]) ])
ylabel(L"$H$ ($\mathrm{m}$)",color="red",fontsize=fs)
setp(ax1.get_xticklabels(),visible=false)
#anchored_text = annotate("ii", fontsize= fs*1.5, xy=(0.025, 0.8), xycoords="axes fraction")
tick_params(axis="both",labelsize=fs*0.8)

ax2 = ax1.twinx()
ax2.spines["right"]
ylim([0.1, maximum((sed_source[:,check_pt]*365*day./width_)./(sgrid[2]-sgrid[1])*1e3)])
plot(sgrid[1:end-1] .+ 0.5*(sgrid[1]-sgrid[2]), (sed_source[:,check_pt]*365*day./width_)./(sgrid[2]-sgrid[1])*1e3,"black",linewidth=2)
setp(ax2.get_xticklabels(),visible=false)
ylabel(L"ė (mm a$^{-1}$)",color="black",fontsize=fs)
tick_params(axis="both",labelsize=fs*0.8 )

ax3 = subplot(3,1,3, sharex=ax)
#plot(sgrid, -Qbes[:,check_pt], linewidth=2,color="darkgrey");
plot(sgrid, -Qws[:,check_pt],"blue",linewidth=2)
ylim([0.1,  19 ])
setp(ax2.get_xticklabels(),visible=false)
ylabel(L"Q$_{w}$  (m$^{3}$ s$^{-1}$)",color="blue",fontsize=fs)
xlabel(L"$x$  (km)",fontsize=fs)
tick_params(axis="both",labelsize=fs*0.8 )

ax4 = ax3.twinx()
ax4.spines["right"]
plot(sgrid, -Qbs[:,check_pt], linewidth=2, color = "red")
xlim([0,maximum(sgrid)])
ylim([0.0001, 0.0075 ])
ylabel(L"Q$_s$  (m$^{3}$ s$^{-1}$)",color = "red",fontsize=fs)
tick_params(axis="both",labelsize=fs*0.8 )
#anchored_text = annotate("iii", fontsize= fs*1.5, xy=(0.025, 0.8), xycoords="axes fraction")

tight_layout()
subplots_adjust(wspace=0,hspace=0)


savefig("process_in_model_poster.pdf")
