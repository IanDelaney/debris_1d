
# Bench-like synthetic run
using Parameters
ENV["PYTHON"]=""

using PyPlot
using Dierckx
using DifferentialEquations
import Roots: find_zero
using Dates
using JLD2
using DelimitedFiles
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, day, year,spinner, Num, Phys

if homedir()== "/Users/Ian"
    fp_save="/Users/Ian/data/debris/"
    ice_data_loc="/Users/Ian/data/debris/"
elseif homedir()=="/users/idelaney"
    fp_save="/scratch/idelaney/data/debris/"
    ice_data_loc="/scratch/idelaney/data/debris/"
    using Pkg; Pkg.activate(".")
end

spin_threshold = 1e-10; break_time=Int(2)

include("interface-ice.jl")
### helper functions and SUGSET updates for this runner    
#############################
### run model and process ###
#############################
file_name="dm_test_0015"

run_name=[ "clean"  "debris" "debris_cleanmelt" ][2]
pg = Mountain(
              tspan_spin =(0,year),
              tspan=(0,year)
              )
include("file_load.jl")


pg = Mountain(pg,
              stoch= false,
              
              lake_level= 0.0,
              till_growth_lim =0.25,
              till_lim =0.5,
              sgrid=range(pg.domain[1],stop=pg.domain[2],length=350), #350 is "normal"#
              )

include("interface-ice.jl")
pp=Phys(pp,
        Dm=0.01,
        Kg= 5e-6,
        #fi=3,
        #ft=3,
        #hookeangle=π/10
        )

ϵ̇ = SUGSET.source_till.(pg.sgrid, Ref(0.), Ref(pg), Ref(pp))

pg = Mountain(pg,
              ht0 = ϵ̇[1:end-1]*0.0 .+ 5e-5,
              ds= abs(pg.sgrid[2]-pg.sgrid[1]),
            
            )

pn =Num(Δσ=1e-3)

hts = pg.ht0; Qbs = pg.sgrid*0.0
@time hts, Qbs, spins  =   one_eval(pg,pp,pn,spin_threshold,break_time);


Qw  = -SUGSET.Q_w(pg.sgrid, 0.0, pg, pp)
zs_ = SUGSET.zs.(pg.sgrid, Ref(0.0) , Ref(pg))
zb_ = SUGSET.zb.(pg.sgrid, Ref(0.0) , Ref(pg))
ϵ̇   = SUGSET.source_till.(pg.sgrid, Ref(0.0), Ref(pg), Ref(pp))
ṁt  =  SUGSET.effective_sed_source.(pg.sgrid[1:end-1], Ref(0.0), Ref(pg), Ref(pp), Ref(pn), hts )
velocity  = Qw  ./ SUGSET.xsect(pg.sgrid, 0.0, pg, pp, pn)[1]
∂ϕ_∂x = SUGSET.Ψˣ.(pg.sgrid,Ref(0.0),Ref(pg),Ref(pp))
ub  = SUGSET.basal_velocity.(pg.sgrid, Ref(0.0), Ref(pg),Ref(pp))
surf_debris = surf_deb_t.(pg.sgrid, Ref(0.0), Ref(pg))

if run_name=="clean"
    ϵ̇_print   = mean(ϵ̇[200:length(pg.sgrid)]*year)
    mt_print  = sum(ṁt[200:length(pg.sgrid)-1]*year)*(pg.sgrid[2] - pg.sgrid[1])*width(0.0,0.0,pg)
    Qb_end    = Qbs[200]
    glac_area = mean(width.(pg.sgrid[1:200],Ref(0.0),Ref(pg)))*(abs(pg.sgrid[end]-pg.sgrid[200]))
elseif run_name=="debris"
    ϵ̇_print  = mean(ϵ̇[60:length(pg.sgrid)]*year)
    mt_print = sum(ṁt[60:length(pg.sgrid)-1]*year)*(pg.sgrid[2] - pg.sgrid[1])*width(0.0,0.0,pg)
    Qb_end   = Qbs[60]
    glac_area = mean(width.(pg.sgrid[1:60],Ref(0.0),Ref(pg)))*(abs(pg.sgrid[end]-pg.sgrid[60]))
end


println("
        $(run_name) glacier
    
        Erosion rate: $(round(ϵ̇_print*1e3,digits=4)) mm a⁻¹ no bedrock limitation
       
        Till production:  $(round(mt_print,digits=4)) m³ a⁻¹ 
            area-scaled:  $(round(mt_print*1e3/glac_area,digits=4)) mm a⁻¹
       
        Sediment discharge: $(round(Qb_end,digits=4)) m³ s⁻¹

    ")
plot(pg.sgrid,-Qw)

error()
figure(1)
subplot(4,1,1)
p1=plot(pg.sgrid,abs.(velocity))

ylabel(" H₂O vel. (m s⁻¹)")
title("$(run_name) glacier")

p1b = twinx()
p1=plot(pg.sgrid,surf_debris, color="grey")
ylabel("Debris (m)", color="grey")

subplot(4,1,2)
plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]), ṁt*year)
ylabel("ṁt (m a⁻¹)")

subplot(4,1,3)
plot(pg.sgrid,-Qw)
ylabel("Qw (m³ s⁻¹)")

subplot(4,1,4)
semilogy(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]),hts)
#plot(pg.sgrid[1:end-1] .-(pg.sgrid[2] - pg.sgrid[1]),hts)
ylabel("till (m)")
axhline(0)
tight_layout()
subplots_adjust(hspace=0.0)


@save "$(fp_save)$(run_name)_$(pp.Dm)_outputs.jld2" hts Qw Qbs zs_ zb_ ϵ̇ ṁt velocity ub ∂ϕ_∂x spins  pg pp pn        




